﻿using SGIF.Datos;
using SGIF.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Presentacion
{
    public partial class FrmTrabajador : Form
    {
        public static DataTable dt = new DataTable();

        public FrmTrabajador()
        {
            InitializeComponent();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void FrmTrabajador_Load(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = FTrabajador.GetAll();

                dt = ds.Tables[0];
                dgvTrabajador.DataSource = dt;
                //Si hay datos no mostramos la etiqueta de "no se encontraron datos" 
                if (dt.Rows.Count > 0)
                {

                    lblDatosNoEncontrados.Visible = false;
                    dgvTrabajador_CellClick(null, null);

                }
                else
                {
                    lblDatosNoEncontrados.Visible = true;
                }

                mostrarGuardarCancelar(false);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

             
            }



        }

        //Metodo mostrarGuardar
        public void mostrarGuardarCancelar(Boolean b)
        {
            btnGuardar.Visible = b;
            btnCancelar.Visible = b;
            btnEditar.Visible = !b;
            btnEliminar.Visible = !b;
            btnNuevo.Visible = !b;
            //La grid con los registros tampoco deberia poder modificarse si se esta agregando uno nuevo o editando
            dgvTrabajador.Enabled = !b;

            //No puedo modificar los datos
            txtNombre.Enabled = b;
            txtApellido.Enabled = b;
            txtTipoDoc.Enabled = b;
            txtNumeroDoc.Enabled = b;
            txtDireccion.Enabled = b;
            txtTelefono.Enabled = b;
            txtFechaNac.Enabled = b;
        }
        //funcion para validar datos obligatorios
        public string validarDatos()
        {
            string resultado = "";
            if (txtNombre.Text == "")
            {
                resultado = resultado + "Nombre \n";
            }
            if (txtApellido.Text == "")
            {

                resultado = resultado + "Apellido \n";
            }

            return resultado;

        }


        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                //Validamos antes de guardar
                string sresultado = validarDatos();
                if (sresultado == "")
                {
                    //Es importante saber si el guardar procede de una modificacion o de un nuevo registro
                    if (txtId.Text.Equals(""))//Si se esta creando uno nuevo
                    {

                        Trabajador trabajador = new Trabajador();
                        trabajador.Nombre = txtNombre.Text;
                        trabajador.Apellido = txtApellido.Text;
                        trabajador.Direccion = txtDireccion.Text;
                        trabajador.Telefono = txtTelefono.Text;
                        trabajador.TipoDoc = Convert.ToInt32(txtTipoDoc.Text);
                        trabajador.NumeroDoc = Convert.ToInt32(txtNumeroDoc.Text);
                        trabajador.FechaNacimiento = Convert.ToDateTime(txtFechaNac.Text);

                        if (FTrabajador.Insertar(trabajador) > 0)
                        {
                            MessageBox.Show("Datos insertados correctamente");
                            //Llamamos el formulario load
                            //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                            //Mostrar nuevamente actualizado los registros
                            FrmTrabajador_Load(null, null);


                        }
                    }
                    //Si se esta modificando un registro
                    else
                    {
                        //Se hace lo mismo excepto lo de considerar el id
                        Trabajador trabajador = new Trabajador();
                        trabajador.Id = Convert.ToInt32(txtId.Text);
                        trabajador.Nombre = txtNombre.Text;
                        trabajador.Apellido = txtApellido.Text;
                        trabajador.Direccion = txtDireccion.Text;
                        trabajador.Telefono = txtTelefono.Text;
                        trabajador.TipoDoc = Convert.ToInt32(txtTipoDoc.Text);
                        trabajador.NumeroDoc = Convert.ToInt32(txtNumeroDoc.Text);
                        trabajador.FechaNacimiento = Convert.ToDateTime(txtFechaNac.Text);

                        if (FTrabajador.Actualizar(trabajador) > 0)
                        {
                            MessageBox.Show("Datos Modificados correctamente");
                            //Llamamos el formulario load
                            //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                            //Mostrar nuevamente actualizado los registros
                            FrmTrabajador_Load(null, null);


                        }


                    }//Fin del else interno
                }
                else
                {
                    MessageBox.Show("Faltan cargar datos: \n" + sresultado);

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }


        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            //Al escoger la opcion de nuevo deberia solo ser visible el boton de guardar o cancelar la crecion del nuevo registro
            mostrarGuardarCancelar(true);

            //Limpiamos las casillas
            txtId.Text = "";
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtTipoDoc.Text = "";
            txtNumeroDoc.Text = "";
            txtTelefono.Text = "";
            txtDireccion.Text = "";
            txtFechaNac.Text = "";
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(false);
            dgvTrabajador_CellClick(null, null);

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(true);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                //Verificamos que realmente quiere eliminar los registros seleccionados
                if (MessageBox.Show("Realmente desea eliminar los Trabajadores seleccionados?", "Eliminacion de Trabajador", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    //Recorrer y obtener todos los id de trabajadores donde este checkeado
                    foreach (DataGridViewRow row in dgvTrabajador.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells["Eliminar"].Value))
                        {
                            //Si esta checkeado
                            Trabajador trabajador = new Trabajador();
                            //NOTA: Verificar que la columna si sea IdTrabajador---------------
                            trabajador.Id = Convert.ToInt32(row.Cells["IdTrabajador"].Value);
                            if (FTrabajador.Eliminar(trabajador) != 1)
                            {
                                MessageBox.Show("El trabajador no pudo ser eliminado", "Eliminacion de trabajador", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                            }

                        }


                    }

                    //Volvemos a cargar la lista de registros una vez se han eliminado
                    FrmTrabajador_Load(null, null);
                }//Fin del if





            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }



        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //copia table de registros en dv y filtra sobre dv
                DataView dv = new DataView(dt.Copy());
                dv.RowFilter = cmbBuscar.Text + " Like '%" + txtBuscar.Text + "%'";
                dgvTrabajador.DataSource = dv;
                if (dv.Count == 0)
                {
                    lblDatosNoEncontrados.Visible = true;
                }
                else
                {
                    lblDatosNoEncontrados.Visible = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);


            }

        }

        private void dgvTrabajador_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvTrabajador.CurrentRow != null)
            {
                txtId.Text = dgvTrabajador.CurrentRow.Cells[1].Value.ToString();
                txtNombre.Text = dgvTrabajador.CurrentRow.Cells[2].Value.ToString();
                txtApellido.Text = dgvTrabajador.CurrentRow.Cells[3].Value.ToString();
                txtTipoDoc.Text = dgvTrabajador.CurrentRow.Cells[4].Value.ToString();
                txtNumeroDoc.Text = dgvTrabajador.CurrentRow.Cells[5].Value.ToString();
                txtTelefono.Text = dgvTrabajador.CurrentRow.Cells[6].Value.ToString();
                txtDireccion.Text = dgvTrabajador.CurrentRow.Cells[7].Value.ToString();
                txtFechaNac.Text = dgvTrabajador.CurrentRow.Cells[8].Value.ToString();
            }
        }

        private void dgvTrabajador_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Verificar que el click vino de la checkbox
            if (e.ColumnIndex == dgvTrabajador.Columns["Eliminar"].Index)
            {
                DataGridViewCheckBoxCell chkEliminar =
                (DataGridViewCheckBoxCell)dgvTrabajador.Rows[e.RowIndex].Cells["Eliminar"];
                //Se le pone el valor opuesto seleccionado-->DesSeleccionado
                chkEliminar.Value = !Convert.ToBoolean(chkEliminar.Value);

            }



        }
    }
}
