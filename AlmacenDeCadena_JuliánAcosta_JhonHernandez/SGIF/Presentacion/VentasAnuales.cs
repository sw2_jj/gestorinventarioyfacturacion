﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Presentacion
{
    public partial class VentasAnuales : Form
    {
        public VentasAnuales()
        {
            InitializeComponent();
        }

        private void VentasAnuales_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'ventasAnuales1.ViewVentasAnuales' Puede moverla o quitarla según sea necesario.
            this.viewVentasAnualesTableAdapter.Fill(this.ventasAnuales1.ViewVentasAnuales);

        }
    }
}
