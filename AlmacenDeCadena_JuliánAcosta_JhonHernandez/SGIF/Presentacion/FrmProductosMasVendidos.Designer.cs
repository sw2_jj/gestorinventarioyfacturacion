﻿namespace SGIF.Presentacion
{
    partial class FrmProductosMasVendidos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.productosMasVendidos = new SGIF.ProductosMasVendidos();
            this.productosMasVendidosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productosPorProveedor = new SGIF.ProductosPorProveedor();
            this.productosXProveedorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productos_x_ProveedorTableAdapter = new SGIF.ProductosPorProveedorTableAdapters.Productos_x_ProveedorTableAdapter();
            this.productosMasVendidosBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.productosMasVendidosTableAdapter = new SGIF.ProductosMasVendidosTableAdapters.productosMasVendidosTableAdapter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.productosMasVendidosBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.idProductoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcionProductoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioUnitarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalVendidoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.productosMasVendidos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productosMasVendidosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productosPorProveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productosXProveedorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productosMasVendidosBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productosMasVendidosBindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // productosMasVendidos
            // 
            this.productosMasVendidos.DataSetName = "ProductosMasVendidos";
            this.productosMasVendidos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // productosMasVendidosBindingSource
            // 
            this.productosMasVendidosBindingSource.DataSource = this.productosMasVendidos;
            this.productosMasVendidosBindingSource.Position = 0;
            // 
            // productosPorProveedor
            // 
            this.productosPorProveedor.DataSetName = "ProductosPorProveedor";
            this.productosPorProveedor.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // productosXProveedorBindingSource
            // 
            this.productosXProveedorBindingSource.DataMember = "Productos x Proveedor";
            this.productosXProveedorBindingSource.DataSource = this.productosPorProveedor;
            // 
            // productos_x_ProveedorTableAdapter
            // 
            this.productos_x_ProveedorTableAdapter.ClearBeforeFill = true;
            // 
            // productosMasVendidosBindingSource1
            // 
            this.productosMasVendidosBindingSource1.DataMember = "productosMasVendidos";
            this.productosMasVendidosBindingSource1.DataSource = this.productosMasVendidos;
            // 
            // productosMasVendidosTableAdapter
            // 
            this.productosMasVendidosTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idProductoDataGridViewTextBoxColumn,
            this.descripcionProductoDataGridViewTextBoxColumn,
            this.precioUnitarioDataGridViewTextBoxColumn,
            this.totalVendidoDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.productosMasVendidosBindingSource2;
            this.dataGridView1.Location = new System.Drawing.Point(181, 150);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(447, 266);
            this.dataGridView1.TabIndex = 0;
            // 
            // productosMasVendidosBindingSource2
            // 
            this.productosMasVendidosBindingSource2.DataMember = "productosMasVendidos";
            this.productosMasVendidosBindingSource2.DataSource = this.productosMasVendidos;
            // 
            // idProductoDataGridViewTextBoxColumn
            // 
            this.idProductoDataGridViewTextBoxColumn.DataPropertyName = "idProducto";
            this.idProductoDataGridViewTextBoxColumn.HeaderText = "idProducto";
            this.idProductoDataGridViewTextBoxColumn.Name = "idProductoDataGridViewTextBoxColumn";
            // 
            // descripcionProductoDataGridViewTextBoxColumn
            // 
            this.descripcionProductoDataGridViewTextBoxColumn.DataPropertyName = "descripcionProducto";
            this.descripcionProductoDataGridViewTextBoxColumn.HeaderText = "descripcionProducto";
            this.descripcionProductoDataGridViewTextBoxColumn.Name = "descripcionProductoDataGridViewTextBoxColumn";
            // 
            // precioUnitarioDataGridViewTextBoxColumn
            // 
            this.precioUnitarioDataGridViewTextBoxColumn.DataPropertyName = "PrecioUnitario";
            this.precioUnitarioDataGridViewTextBoxColumn.HeaderText = "PrecioUnitario";
            this.precioUnitarioDataGridViewTextBoxColumn.Name = "precioUnitarioDataGridViewTextBoxColumn";
            // 
            // totalVendidoDataGridViewTextBoxColumn
            // 
            this.totalVendidoDataGridViewTextBoxColumn.DataPropertyName = "TotalVendido";
            this.totalVendidoDataGridViewTextBoxColumn.HeaderText = "TotalVendido";
            this.totalVendidoDataGridViewTextBoxColumn.Name = "totalVendidoDataGridViewTextBoxColumn";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Monotype Corsiva", 24.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label2.Location = new System.Drawing.Point(241, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(310, 40);
            this.label2.TabIndex = 4;
            this.label2.Text = "Productos Mas Vendidos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 30F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(184, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(426, 49);
            this.label1.TabIndex = 3;
            this.label1.Text = "ALMACEN DE CADENA";
            // 
            // FrmProductosMasVendidos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FrmProductosMasVendidos";
            this.Text = "FrmProductosMasVendidos";
            this.Load += new System.EventHandler(this.FrmProductosMasVendidos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.productosMasVendidos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productosMasVendidosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productosPorProveedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productosXProveedorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productosMasVendidosBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productosMasVendidosBindingSource2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ProductosMasVendidos productosMasVendidos;
        private System.Windows.Forms.BindingSource productosMasVendidosBindingSource;
        private SGIF.ProductosPorProveedor productosPorProveedor;
        private System.Windows.Forms.BindingSource productosXProveedorBindingSource;
        private ProductosPorProveedorTableAdapters.Productos_x_ProveedorTableAdapter productos_x_ProveedorTableAdapter;
        private System.Windows.Forms.BindingSource productosMasVendidosBindingSource1;
        private ProductosMasVendidosTableAdapters.productosMasVendidosTableAdapter productosMasVendidosTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idProductoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcionProductoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioUnitarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalVendidoDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource productosMasVendidosBindingSource2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}