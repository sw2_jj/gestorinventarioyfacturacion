﻿using SGIF.Datos;
using SGIF.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Presentacion
{
    public partial class FrmFactura : Form
    {
        public static DataTable dt = new DataTable();
        public static string XId, XNombre;
        public FrmFactura()
        {
            InitializeComponent();
        }


        public void setCliente(string id, string nombre)
        {
            txtIdCliente.Text = id;
            txtCliente.Text = nombre;
            XId = id;
            XNombre = nombre;


        }

        private static FrmFactura _instancia=null;

        public static FrmFactura GetInstance()
        {

            if (_instancia == null) _instancia = new FrmFactura();

            return _instancia;

        }


        private void FrmFactura_Load(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = FFactura.GetAll();

                dt = ds.Tables[0];
                dgvFactura.DataSource = dt;
                //Si hay datos no mostramos la etiqueta de "no se encontraron datos" 
                if (dt.Rows.Count > 0)
                {

                    lblDatosNoEncontrados.Visible = false;
                    dgvFactura_CellClick(null, null);

                }
                else
                {
                    lblDatosNoEncontrados.Visible = true;
                }

                mostrarGuardarCancelar(false);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);


            }
        }
        public void mostrarGuardarCancelar(Boolean b)
        {
            btnGuardar.Visible = b;
            btnCancelar.Visible = b;
            btnRefrescar.Visible = b;
            btnEditar.Visible = !b;
            btnNuevo.Visible = !b;
            btnBuscarCliente.Visible = b;
            //La grid con los registros tampoco deberia poder modificarse si se esta agregando uno nuevo o editando
            dgvFactura.Enabled = !b;

            //No puedo modificar los datos
            txtFecha.Enabled = b;
            txtCliente.Enabled = b;
            btnBuscarCliente.Enabled = b;
            txtIdCliente.Enabled = b;


        }
        //funcion para validar datos obligatorios
        public string validarDatos()
        {
            string resultado = "";
            if (txtFecha.Text == "")
            {
                resultado = resultado + "Fecha \n";
            }
            if (txtCliente.Text == "")
            {

                resultado = resultado + "cliente \n";
            }

            return resultado;

        }
        private void Buscar_Click(object sender, EventArgs e)
        {
            FrmCliente frmCliente= new FrmCliente();
            //1 significa que fue llamado desde el formulario producto
            frmCliente.setFlag("1");
            frmCliente.ShowDialog();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                //Validamos antes de guardar
                string sresultado = validarDatos();
                if (sresultado == "")
                {
                    //Es importante saber si el guardar procede de una modificacion o de un nuevo registro
                    if (txtId.Text.Equals(""))//Si se esta creando uno nuevo
                    {

                        Factura factura = new Factura();
                        factura.FechaVenta = txtFecha.Value;
                        factura.Cliente.Id = Convert.ToInt32(txtIdCliente.Text);
                        
                        int i = FFactura.Insertar(factura);
                        if ( i > 0)
                        {
                            FrmFactura_Load(null, null);
                            factura.Id = i;
                            cargarDetalle(factura);
                            
                        }
                    }
                    //Si se esta modificando un registro
                    else
                    {
                        //Se hace lo mismo excepto lo de considerar el id

                        Factura factura = new Factura();
                        factura.Id= Convert.ToInt32(txtId.Text);
                        factura.FechaVenta = Convert.ToDateTime(txtFecha.Text);
                        factura.Cliente.Id = Convert.ToInt32(txtCliente.Text);


                        if (FFactura.Actualizar(factura) == 1)
                        {
                            MessageBox.Show("Datos Modificados correctamente");
                            //Llamamos el formulario load
                            //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                            //Mostrar nuevamente actualizado los registros
                            FrmFactura_Load(null, null);


                        }


                    }//Fin del else interno
                }
                else
                {
                    MessageBox.Show("Faltan cargar datos: \n" + sresultado);

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }

        }
        private void cargarDetalle(Factura factura)
        {

            //Aqui se llama el formulario que vamos a definir
            FrmProductoXFactura f = FrmProductoXFactura.GetInstance();
            f.setFactura(factura);
            f.ShowDialog();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(false);
            dgvFactura_CellClick(null, null);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(true);
            txtId.Text = "";
            txtFecha.Text = "";
            txtCliente.Text = "";
            txtIdCliente.Text = "";
            

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(true);
        }

        private void dgvFactura_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.ColumnIndex == dgvFactura.Columns["Eliminar"].Index)
            //{
            //    DataGridViewCheckBoxCell chkEliminar =
            //    (DataGridViewCheckBoxCell)dgvFactura.Rows[e.RowIndex].Cells["Eliminar"];
            //    //Se le pone el valor opuesto seleccionado-->DesSeleccionado
            //    chkEliminar.Value = !Convert.ToBoolean(chkEliminar.Value);

            //}
        }

        private void dgvFactura_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvFactura.CurrentRow != null)
            {
                //idFactura	idCliente	fecha	nombre	apellido
                txtId.Text = dgvFactura.CurrentRow.Cells["idFactura"].Value.ToString();
                txtIdCliente.Text = dgvFactura.CurrentRow.Cells["idCliente"].Value.ToString();
                txtFecha.Text = dgvFactura.CurrentRow.Cells["fecha"].Value.ToString();
                txtCliente.Text= dgvFactura.CurrentRow.Cells["nombre"].Value.ToString() ;

            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //copia table de registros en dv y filtra sobre dv
                DataView dv = new DataView(dt.Copy());
                dv.RowFilter = cmbBuscar.Text + " Like '%" + txtBuscar.Text + "%'";
                dgvFactura.DataSource = dv;
                if (dv.Count == 0)
                {
                    lblDatosNoEncontrados.Visible = true;
                }
                else
                {
                    lblDatosNoEncontrados.Visible = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);


            }
        }

        private void cmbBuscar_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = new DataView(dt.Copy());
                dv.RowFilter = cmbBuscar.Text + " Like '%" + txtBuscar.Text + "%'";
                dgvFactura.DataSource = dv;

                if (dv.Count == 0)
                {
                    lblDatosNoEncontrados.Visible = true;
                }
                else
                {
                    lblDatosNoEncontrados.Visible = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);


            }


        }

        private void dgvFactura_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {


            if (dgvFactura.CurrentRow != null)
            {
                Factura factura = new Factura();
                factura.Id = Convert.ToInt32(dgvFactura.CurrentRow.Cells["idFactura"].Value.ToString());
                factura.Cliente.Id = Convert.ToInt32(dgvFactura.CurrentRow.Cells["idCliente"].Value.ToString());
                factura.FechaVenta=Convert.ToDateTime(dgvFactura.CurrentRow.Cells["fecha"].Value.ToString());
                factura.Cliente.Nombre = dgvFactura.CurrentRow.Cells["nombre"].Value.ToString();

                cargarDetalle(factura);

            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtIdCliente.Text = XId;
            txtCliente.Text = XNombre;
        }

        private void txtCliente_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
