﻿using SGIF.Datos;
using SGIF.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Presentacion
{
    public partial class FrmCargo : Form
    {

        public static DataTable dt = new DataTable();
        public FrmCargo()
        {
            InitializeComponent();
        }

        //Creamos un metodo o funcion para ocultar los demas botones innecesarios al realizar una operacion
        public void mostrarGuardarCancelar(Boolean b)
        {
            btnGuardar.Visible = b;
            btnCancelar.Visible = b;
            btnEditar.Visible = !b;
            btnEliminar.Visible = !b;
            btnNuevo.Visible = !b;
            //La grid con los registros tampoco deberia poder modificarse si se esta agregando uno nuevo o editando
            dgvCargo.Enabled = !b;

            //No puedo modificar los datos
            txtDescripcionCargo.Enabled = b;
            txtSalario.Enabled = b;

        }

        //funcion para validar datos obligatorios
        public string validarDatos()
        {
            string resultado = "";
            if (txtDescripcionCargo.Text == "")
            {
                resultado = resultado + "Nombre \n";
            }
            if (txtSalario.Text == "")
            {

                resultado = resultado + "Apellido \n";
            }

            return resultado;

        }

        //Cargar datos/Registros
        private void FrmCargo_Load(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = FCargo.GetAll();
                dt = ds.Tables[0];
                dgvCargo.DataSource = dt;
                //Si hay datos no mostramos la etiqueta de "no se encontraron datos" 
                if (dt.Rows.Count > 0)
                {

                    lblDatosNoEncontrados.Visible = false;
                    dgvCargo_CellClick(null, null);

                }
                else
                {
                    lblDatosNoEncontrados.Visible = true;
                }

                mostrarGuardarCancelar(false);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

                throw;
            }



        }


        //Cuando seleccionemos debemos pasar los datos del registro a los campos de texto
        private void dgvCargo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvCargo.CurrentRow != null)
            {
                txtId.Text = dgvCargo.CurrentRow.Cells[1].Value.ToString();
                txtDescripcionCargo.Text = dgvCargo.CurrentRow.Cells[2].Value.ToString();
                txtSalario.Text = dgvCargo.CurrentRow.Cells[3].Value.ToString();
                            }
        }

        //Evento proveniente de la grilla
        private void dgvCargo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Verificar que el click vino de la checkbox
            if (e.ColumnIndex == dgvCargo.Columns["Eliminar"].Index)
            {
                DataGridViewCheckBoxCell chkEliminar =
                (DataGridViewCheckBoxCell)dgvCargo.Rows[e.RowIndex].Cells["Eliminar"];
                //Se le pone el valor opuesto seleccionado-->DesSeleccionado
                chkEliminar.Value = !Convert.ToBoolean(chkEliminar.Value);

            }

        }


        private void btnGuardar_Click(object sender, EventArgs e)
        {

            try
            {
                //Validamos antes de guardar
                string sresultado = validarDatos();
                if (sresultado == "")
                {
                    //Es importante saber si el guardar procede de una modificacion o de un nuevo registro
                    if (txtId.Text.Equals(""))//Si se esta creando uno nuevo
                    {

                        Cargo cargo = new Cargo();
                        cargo.DescripcionCargo = txtDescripcionCargo.Text;                       
                        cargo.Salario = Convert.ToInt32(txtSalario.Text);

                        if (FCargo.Insertar(cargo) > 0)
                        {
                            MessageBox.Show("Datos insertados correctamente");
                            //Llamamos el formulario load
                            //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                            //Mostrar nuevamente actualizado los registros
                            FrmCargo_Load(null, null);


                        }
                    }
                    //Si se esta modificando un registro
                    else
                    {
                        //Se hace lo mismo excepto lo de considerar el id
                        Cargo cargo = new Cargo();
                        cargo.IdCargo = Convert.ToInt32(txtId.Text);
                        cargo.DescripcionCargo = txtDescripcionCargo.Text;
                        cargo.Salario = Convert.ToInt32(txtSalario.Text);


                        if (FCargo.Actualizar(cargo) > 0)
                        {
                            MessageBox.Show("Datos Modificados correctamente");
                            //Llamamos el formulario load
                            //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                            //Mostrar nuevamente actualizado los registros
                            FrmCargo_Load(null, null);


                        }


                    }//Fin del else interno
                }
                else
                {
                    MessageBox.Show("Faltan cargar datos: \n" + sresultado);

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }
        }//Fin boton guardar

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            //Al escoger la opcion de nuevo deberia solo ser visible el boton de guardar o cancelar la crecion del nuevo registro
            mostrarGuardarCancelar(true);

            //Limpiamos las casillas
            txtId.Text = "";
            txtDescripcionCargo.Text = "";
            txtSalario.Text = "";
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(true);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(false);
            dgvCargo_CellClick(null, null);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                //Verificamos que realmente quiere eliminar los registros seleccionados
                if (MessageBox.Show("Realmente desea eliminar los cargos seleccionados?", "Eliminacion de cargo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    //Recorrer y obtener todos los id de cargos donde este checkeado
                    foreach (DataGridViewRow row in dgvCargo.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells["Eliminar"].Value))
                        {
                            //Si esta checkeado
                            Cargo cargo = new Cargo();
                            cargo.IdCargo = Convert.ToInt32(row.Cells["IdCargo"].Value);
                            if (FCargo.Eliminar(cargo) != 1)
                            {
                                MessageBox.Show("El cargo no pudo ser eliminado", "Eliminacion de cargo", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                            }

                        }


                    }

                    //Volvemos a cargar la lista de registros una vez se han eliminado
                    FrmCargo_Load(null, null);
                }//Fin del if
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = new DataView(dt.Copy());
                dv.RowFilter = cmbBuscar.Text + " Like '%" + txtBuscar.Text + "%'";
                dgvCargo.DataSource = dv;

                if (dv.Count == 0)
                {
                    lblDatosNoEncontrados.Visible = true;
                }
                else
                {
                    lblDatosNoEncontrados.Visible = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);


            }
        }
    }
}
