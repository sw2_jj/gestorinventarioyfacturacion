﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF
{
    public partial class FrmReporteFactura : Form
    {
        public FrmReporteFactura()
        {
            InitializeComponent();
        }

        public void setFacturaId(int facturaId) {
            txtFacturaId.Text = facturaId.ToString();
        }

        private void FrmReporteFactura_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSetPrincipal.usp_Reportes_GenerarReporteFactura' table. You can move, or remove it, as needed.
            this.usp_Reportes_GenerarReporteFacturaTableAdapter.Fill(this.DataSetPrincipal.usp_Reportes_GenerarReporteFactura,Convert.ToInt32(txtFacturaId.Text));

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }

        private void txtFacturaId_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
