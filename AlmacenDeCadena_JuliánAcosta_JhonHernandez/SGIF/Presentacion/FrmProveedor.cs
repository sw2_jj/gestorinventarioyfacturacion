﻿using SGIF.Datos;
using SGIF.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Presentacion
{
    
    public partial class FrmProveedor : Form
    {
        public static DataTable dt = new DataTable();
        public FrmProveedor()
        {
            InitializeComponent();
        }

        private void dgvProveedor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Verificar que el click vino de la checkbox
            if (e.ColumnIndex == dgvProveedor.Columns["Eliminar"].Index)
            {
                DataGridViewCheckBoxCell chkEliminar =
                (DataGridViewCheckBoxCell)dgvProveedor.Rows[e.RowIndex].Cells["Eliminar"];
                //Se le pone el valor opuesto seleccionado-->DesSeleccionado
                chkEliminar.Value = !Convert.ToBoolean(chkEliminar.Value);

            }
        }

        private void dgvProveedor_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvProveedor.CurrentRow != null)
            {
                txtNit.Text = dgvProveedor.CurrentRow.Cells[1].Value.ToString();
                txtRazonSocial.Text = dgvProveedor.CurrentRow.Cells[2].Value.ToString();
                txtRepresentanteLegal.Text = dgvProveedor.CurrentRow.Cells[3].Value.ToString();
                txtDirecion.Text = dgvProveedor.CurrentRow.Cells[4].Value.ToString();
                txtTel.Text = dgvProveedor.CurrentRow.Cells[5].Value.ToString();
                txtFax.Text = dgvProveedor.CurrentRow.Cells[6].Value.ToString();
                
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                //Validamos antes de guardar
                string sresultado = validarDatos();
                if (sresultado == "" )
                {
        
                        //Se hace lo mismo excepto lo de considerar el id
                        Proveedor proveedor = new Proveedor();
                        proveedor.Nit = Convert.ToInt32(txtNit.Text);
                        proveedor.RozonSocial = txtRazonSocial.Text;
                        proveedor.RepresentanteLegal = txtRepresentanteLegal.Text;
                        proveedor.Direccion = txtDirecion.Text;
                        proveedor.Telefono = txtTel.Text;
                        proveedor.Fax = txtFax.Text;


                    if (FProveedor.Actualizar(proveedor) > 0)
                    {
                        MessageBox.Show("Datos Modificados correctamente");
                        //Llamamos el formulario load
                        //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                        //Mostrar nuevamente actualizado los registros
                        FrmProveedor_Load(null, null);


                    }
                    else if (FProveedor.Insertar(proveedor) > 0) {
                        MessageBox.Show("Datos Insertados correctamente");
                        //Llamamos el formulario load
                        //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                        //Mostrar nuevamente actualizado los registros
                        FrmProveedor_Load(null, null);

                    }


                 
                }
                else
                {
                    MessageBox.Show("Faltan cargar datos: \n" + sresultado);

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }
        }
        public void SetFlag(string valor)
        {
            txtFlag.Text = valor;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(false);
            dgvProveedor_CellClick(null, null);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            //Al escoger la opcion de nuevo deberia solo ser visible el boton de guardar o cancelar la crecion del nuevo registro
            mostrarGuardarCancelar(true);

            //Limpiamos las casillas
            txtNit.Text = "";
            txtRazonSocial.Text = "";
            txtRepresentanteLegal.Text = "";
            txtDirecion.Text = "";
            txtTel.Text = "";
            txtFax.Text = "";
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(true);
            txtNit.Enabled = false;
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = new DataView(dt.Copy());
                dv.RowFilter = cmbBuscar.Text + " Like '%" + txtBuscar.Text + "%'";
                dgvProveedor.DataSource = dv;
                if (dv.Count == 0)
                {
                    lblDatosNoEncontrados.Visible = true;
                }
                else
                {
                    lblDatosNoEncontrados.Visible = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);


            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                //Verificamos que realmente quiere eliminar los registros seleccionados
                if (MessageBox.Show("Realmente desea eliminar el proveedor seleccionados?", "Eliminacion de cliente", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    //Recorrer y obtener todos los id de clientes donde este checkeado
                    foreach (DataGridViewRow row in dgvProveedor.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells["Eliminar"].Value))
                        {
                            //Si esta checkeado
                            Proveedor proveedor = new Proveedor();
                            //NOTA: Verificar que la columna si sea IdCliente---------------
                            proveedor.Nit = Convert.ToInt32(row.Cells["nit"].Value);
                            if (FProveedor.Eliminar(proveedor) != 1)
                            {
                                MessageBox.Show("El proveedor no pudo ser eliminado", "Eliminacion de cliente", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                            }

                        }


                    }

                    //Volvemos a cargar la lista de registros una vez se han eliminado
                    FrmProveedor_Load(null, null);
                }//Fin del if





            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void FrmProveedor_Load(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = FProveedor.GetAll();
                dt = ds.Tables[0];
                dgvProveedor.DataSource = dt;
                //Si hay datos no mostramos la etiqueta de "no se encontraron datos" 
                if (dt.Rows.Count > 0)
                {

                    lblDatosNoEncontrados.Visible = false;
                    dgvProveedor_CellClick(null, null);

                }
                else
                {
                    lblDatosNoEncontrados.Visible = true;
                }

                mostrarGuardarCancelar(false);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

                throw;
            }
        }
        public void mostrarGuardarCancelar(Boolean b)
        {
            btnGuardar.Visible = b;
            btnCancelar.Visible = b;
            btnEditar.Visible = !b;
            btnEliminar.Visible = !b;
            btnNuevo.Visible = !b;
            //La grid con los registros tampoco deberia poder modificarse si se esta agregando uno nuevo o editando
            dgvProveedor.Enabled = !b;

            //No puedo modificar los datos
            txtNit.Enabled = b;
            txtRazonSocial.Enabled = b;
            txtRepresentanteLegal.Enabled = b;
            txtDirecion.Enabled = b;
            txtTel.Enabled = b;
            txtFax.Enabled = b;
        }
        public string validarDatos() {
            string resultado="";
            if (txtNit.Text == "") {
                resultado = resultado + "Nit \n";
            }
            if (txtRepresentanteLegal.Text == "")
            {

                resultado = resultado + "Representante Legal \n";
            }

            return resultado;

        }

        private void dgvProveedor_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (txtFlag.Text == "1")
            {
                FrmProducto produc = FrmProducto.GetInstance();
                if (dgvProveedor.CurrentRow != null)
                {
                    produc.setProveedor(dgvProveedor.CurrentRow.Cells[1].Value.ToString(), dgvProveedor.CurrentRow.Cells[2].Value.ToString());
                    produc.Show();
                    Close();



                }
            }
            }

        private void txtFlag_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
