﻿using SGIF.Entidades;
using SisVenttas.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Datos
{
    public class FProveedor
    {
        public static DataSet GetAll()
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {

                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            return FDBHelper.ExecuteDataSet("usp_Data_FProveedor_GetAll", dbParams);

        }

        //metodo Insertar

        public static int Insertar(Proveedor proveedor)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@nit",SqlDbType.Int,0,proveedor.Nit),
                    FDBHelper.MakeParam("@razonSocial",SqlDbType.VarChar,0,proveedor.RozonSocial),
                    FDBHelper.MakeParam("@representanteLegal",SqlDbType.VarChar,0,proveedor.RepresentanteLegal),
                    FDBHelper.MakeParam("@direccion",SqlDbType.VarChar,0,proveedor.Direccion),
                    FDBHelper.MakeParam("@telefono",SqlDbType.VarChar,0,proveedor.Telefono),
                    FDBHelper.MakeParam("@Fax",SqlDbType.VarChar,0,proveedor.Fax)
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FProveedor_Insertar", dbParams));

        }

        //Metodo Actualizar
        public static int Actualizar(Proveedor proveedor )
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@nit",SqlDbType.VarChar,0,proveedor.Nit),
                    FDBHelper.MakeParam("@razonSocial",SqlDbType.VarChar,0,proveedor.RozonSocial),
                    FDBHelper.MakeParam("@representanteLegal",SqlDbType.VarChar,0,proveedor.RepresentanteLegal),
                    FDBHelper.MakeParam("@direccion",SqlDbType.VarChar,0,proveedor.Direccion),
                    FDBHelper.MakeParam("@telefono",SqlDbType.VarChar,0,proveedor.Telefono),
                    FDBHelper.MakeParam("@Fax",SqlDbType.VarChar,0,proveedor.Fax)
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos


            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FProveedor_Actualizar", dbParams));

        }

        //Metodo Eliminar
        public static int Eliminar(Proveedor proveedor)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@nit",SqlDbType.Int,0,proveedor.Nit),
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FProveedor_Eliminar", dbParams));

        }
    }
}
