﻿using SGIF.Datos;
using SGIF.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Presentacion
{
    public partial class Frmlogin : Form
    {
        public Frmlogin()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataSet ds = FLogin.ValidarLogin(txtUsuario.Text, txtContraseña.Text);
            DataTable dt = ds.Tables[0];


            if (dt.Rows.Count > 0)
            {
                //Significa que encontro la cuenta 
                Usuario.Apellido = dt.Rows[0]["Apellido"].ToString();
                Usuario.Nombre = dt.Rows[0]["Nombre"].ToString();
                Usuario.Id =Convert.ToInt32(dt.Rows[0]["Id"]);
                Usuario.Dni = Convert.ToInt32(dt.Rows[0]["DNI"]);
                Usuario.UsuarioC = dt.Rows[0]["Usuario"].ToString();
                Usuario.Tipo1 = dt.Rows[0]["Tipo"].ToString();
                Usuario.Telefono = dt.Rows[0]["Telefono"].ToString();
                Usuario.Direccion = dt.Rows[0]["Direccion"].ToString();
                MDIPrincipal mdi = new MDIPrincipal();
                mdi.ShowDialog();
                
                
            }
            else {
                MessageBox.Show("Usuario y/o contraseña incorrecta");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtContraseña.Text= "";
            txtUsuario.Text= "";
        }

        private void Frmlogin_Load(object sender, EventArgs e)
        {
            this.AcceptButton = button1;
        }
    }
}
