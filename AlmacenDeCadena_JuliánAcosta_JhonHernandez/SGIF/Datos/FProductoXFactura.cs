﻿using SGIF.Entidades;
using SisVenttas.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Datos
{
    public static class FProductoXFactura
    {
        public static DataSet GetAll(int idFactura)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {
                    FDBHelper.MakeParam("@idFactura",SqlDbType.Int,0,idFactura)
                };
          
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            return FDBHelper.ExecuteDataSet("usp_Data_FProductoXFactura_GetAll", dbParams);

        }

        //metodo Insertar

        public static int Insertar(ProductoXFactura productoFac)
        {
            SqlParameter[] dbParams = new SqlParameter[]
            //idFactura, idProducto, cantidad
                {//Para este procedimiento si existen atributos
                      //idCompra, idFactura, idProducto, cantidad
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro idCompra, idFactura, idProducto, cantidad
                  
                    FDBHelper.MakeParam("@idFactura",SqlDbType.Int,0,productoFac.Factura.Id),
                    FDBHelper.MakeParam("@idProducto",SqlDbType.Int,0,productoFac.Producto.IdProducto),
                    FDBHelper.MakeParam("@cantidad",SqlDbType.Int,0,productoFac.Cantidad)
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id

            FDBHelper.ExecuteScalar("usp_Data_FProductoXFactura_Insertar", dbParams);
            return 1;

        }

        //Metodo Actualizar
        public static int Actualizar(ProductoXFactura productoFac)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                 //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idCompra",SqlDbType.Int,0,productoFac.IdCompra),
                    FDBHelper.MakeParam("@idFactura",SqlDbType.Int,0,productoFac.Factura.Id),
                    FDBHelper.MakeParam("@idProducto",SqlDbType.Int,0,productoFac.Producto.IdProducto),
                    FDBHelper.MakeParam("@cantidad",SqlDbType.Int,0,productoFac.Cantidad)
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos


            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FProductoXFactura_Actualizar", dbParams));

        }

        //Metodo Eliminar
        public static int Eliminar(ProductoXFactura productoFac)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idFactura",SqlDbType.Int,0,productoFac.Factura.Id),
                    FDBHelper.MakeParam("@idProducto",SqlDbType.Int,0,productoFac.Producto.IdProducto)

                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara registrso afectados (deberia ser 1)
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FProductoXFactura_Eliminar", dbParams));

        }
        public static int disminuirStock(ProductoXFactura productoFac)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idProducto",SqlDbType.Int,0,productoFac.Producto.IdProducto),
                    FDBHelper.MakeParam("@cantidad",SqlDbType.Int,0,productoFac.Cantidad)


                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FProductoXFactura_disminuirStock", dbParams));

        }

        public static int aumentarStock(ProductoXFactura productoFac)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idProducto",SqlDbType.Int,0,productoFac.Producto.IdProducto),
                    FDBHelper.MakeParam("@cantidad",SqlDbType.Int,0,productoFac.Cantidad)


                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FProductoXFactura_aumentarStock", dbParams));

        }



    }
}
