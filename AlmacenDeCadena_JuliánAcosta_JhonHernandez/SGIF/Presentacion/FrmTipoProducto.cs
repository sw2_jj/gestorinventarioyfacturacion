﻿using SGIF.Datos;
using SGIF.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Presentacion
{
    public partial class FrmTipoProducto : Form
    {
        private static FrmTipoProducto _instancia;

        public static FrmTipoProducto GetInstance()
        {

            if (_instancia == null) _instancia = new FrmTipoProducto();

            return _instancia;

        }


        public static DataTable dt = new DataTable();
        public FrmTipoProducto()
        {
            InitializeComponent();
        }

        //Creamos un metodo void para cambiar el estado de la bandera
        public void setFlag(string valor) {
            txtFlag.Text = valor;

        }


        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            //Al escoger la opcion de nuevo deberia solo ser visible el boton de guardar o cancelar la crecion del nuevo registro
            mostrarGuardarCancelar(true);

            //Limpiamos las casillas
            txtId.Text = "";
            txtNombre.Text = "";

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                //Validamos antes de guardar
                string sresultado = validarDatos();
                if (sresultado == "")
                {
                    //Es importante saber si el guardar procede de una modificacion o de un nuevo registro
                    if (txtId.Text.Equals(""))//Si se esta creando uno nuevo
                    {

                        TipoProducto tipoProducto = new TipoProducto();
                        tipoProducto.TipoProduc = txtNombre.Text;
                     

                        if (FTipoProducto.Insertar(tipoProducto) > 0)
                        {
                            MessageBox.Show("Datos insertados correctamente");
                            //Llamamos el formulario load
                            //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                            //Mostrar nuevamente actualizado los registros
                            FrmTipoProducto_Load(null, null);


                        }
                    }
                    //Si se esta modificando un registro
                    else
                    {
                        //Se hace lo mismo excepto lo de considerar el id
                        TipoProducto tipoProducto = new TipoProducto();
                        tipoProducto.Id = Convert.ToInt32(txtId.Text);
                        tipoProducto.TipoProduc = txtNombre.Text;

                        if (FTipoProducto.Actualizar(tipoProducto) > 0)
                        {
                            MessageBox.Show("Datos Modificados correctamente");
                            //Llamamos el formulario load
                            //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                            //Mostrar nuevamente actualizado los registros
                            FrmTipoProducto_Load(null, null);
                        }
                    }//Fin del else interno
                }
                else
                {
                    MessageBox.Show("Faltan cargar datos: \n" + sresultado);
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(false);
            dgvTipoProducto_CellClick(null, null);
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(true);
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = new DataView(dt.Copy());
                dv.RowFilter = cmbBuscar.Text + " Like '%" + txtBuscar.Text + "%'";
                dgvTipoProducto.DataSource = dv;

                if (dv.Count == 0)
                {
                    lblDatosNoEncontrados.Visible = true;
                }
                else
                {
                    lblDatosNoEncontrados.Visible = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);


            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                //Verificamos que realmente quiere eliminar los registros seleccionados
                if (MessageBox.Show("Realmente desea eliminar los tipos de productos seleccionados?", "Eliminacion de tipo producto", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    //Recorrer y obtener todos los id de tipo producto donde este checkeado
                    foreach (DataGridViewRow row in dgvTipoProducto.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells["Eliminar"].Value))
                        {
                            //Si esta checkeado
                            TipoProducto tipoProducto = new TipoProducto();
                            //NOTA: Verificar que la columna si sea IdtipoProduc---------------
                            tipoProducto.Id = Convert.ToInt32(row.Cells["IdTipoProducto"].Value);
                            if (FTipoProducto.Eliminar(tipoProducto) != 1)
                            {
                                MessageBox.Show("El tipo de producto no pudo ser eliminado", "Eliminacion de tipo producto", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                            }

                        }


                    }

                    //Volvemos a cargar la lista de registros una vez se han eliminado
                    FrmTipoProducto_Load(null, null);
                }//Fin del if





            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }

        }

        private void dgvTipoProducto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Verificar que el click vino de la checkbox
            if (e.ColumnIndex == dgvTipoProducto.Columns["Eliminar"].Index)
            {
                DataGridViewCheckBoxCell chkEliminar =
                (DataGridViewCheckBoxCell)dgvTipoProducto.Rows[e.RowIndex].Cells["Eliminar"];
                //Se le pone el valor opuesto seleccionado-->DesSeleccionado
                chkEliminar.Value = !Convert.ToBoolean(chkEliminar.Value);

            }
        }

        private void dgvTipoProducto_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvTipoProducto.CurrentRow != null)
            {
                txtId.Text = dgvTipoProducto.CurrentRow.Cells[1].Value.ToString();
                txtNombre.Text = dgvTipoProducto.CurrentRow.Cells[2].Value.ToString();
            }
        }

        private void FrmTipoProducto_Load(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = FTipoProducto.GetAll();
                dt = ds.Tables[0];
                dgvTipoProducto.DataSource = dt;
                //Si hay datos no mostramos la etiqueta de "no se encontraron datos" 
                if (dt.Rows.Count > 0)
                {

                    lblDatosNoEncontrados.Visible = false;
                    dgvTipoProducto_CellClick(null, null);

                }
                else
                {
                    lblDatosNoEncontrados.Visible = true;
                }

                mostrarGuardarCancelar(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

                throw;
            }

        }
        public void mostrarGuardarCancelar(Boolean b)
        {
            btnGuardar.Visible = b;
            btnCancelar.Visible = b;
            btnEditar.Visible = !b;
            btnEliminar.Visible = !b;
            btnNuevo.Visible = !b;
            //La grid con los registros tampoco deberia poder modificarse si se esta agregando uno nuevo o editando
            dgvTipoProducto.Enabled = !b;

            //No puedo modificar los datos
            txtNombre.Enabled = b;
        }
        //funcion para validar datos obligatorios
        public string validarDatos()
        {
            string resultado = "";
            if (txtNombre.Text == "")
            {
                resultado = resultado + "Nombre \n";
            }

            return resultado;

        }

        private void cmbBuscar_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dgvTipoProducto_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvTipoProducto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (txtFlag.Text == "1")
            {
            FrmProducto f = FrmProducto.GetInstance();
                
            if (dgvTipoProducto.CurrentRow != null)
            {
                
                    //FrmProducto.GetInstance().ShowDialog();
                    
                    f.setCategoria(dgvTipoProducto.CurrentRow.Cells["idTipoProducto"].Value.ToString(), dgvTipoProducto.CurrentRow.Cells["descripcionTipoProducto"].Value.ToString());
                    
                   // f.Show();
                    //txtFlag.Text = "0";
                    Close();
                    
            }

            }




        }

        private void txtFlag_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
