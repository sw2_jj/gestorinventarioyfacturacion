﻿using SGIF.Datos;
using SGIF.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Presentacion
{
    public partial class FrmCliente : Form
    {
        public static DataTable dt = new DataTable();

        public FrmCliente()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        //Cargar datos/Registros
        private void FrmCliente_Load(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = FCliente.GetAll();
                dt = ds.Tables[0];
                dgvClientes.DataSource = dt;
                //Si hay datos no mostramos la etiqueta de "no se encontraron datos" 
                if (dt.Rows.Count > 0)
                {

                    lblDatosNoEncontrados.Visible = false;
                    dgvClientes_CellClick(null, null); 

                }
                else {
                    lblDatosNoEncontrados.Visible = true;
                }

                mostrarGuardarCancelar(false);


            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message + ex.StackTrace);

                throw;
            }
            


        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

            try
            {
                //Validamos antes de guardar
                string sresultado = validarDatos();
                if (sresultado == "")
                {
                    //Es importante saber si el guardar procede de una modificacion o de un nuevo registro
                    if (txtId.Text.Equals(""))//Si se esta creando uno nuevo
                    {

                        Cliente cliente = new Cliente();
                        cliente.Nombre = txtNombre.Text;
                        cliente.Apellido = txtApellido.Text;
                        cliente.Direccion = txtDireccion.Text;
                        cliente.Telefono = txtTelefono.Text;
                        cliente.TipoDoc = Convert.ToInt32(txtTipoDoc.Text);
                        cliente.NumeroDoc = Convert.ToInt32(txtNumeroDoc.Text);

                        if (FCliente.Insertar(cliente) > 0)
                        {
                            MessageBox.Show("Datos insertados correctamente");
                            //Llamamos el formulario load
                            //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                            //Mostrar nuevamente actualizado los registros
                            FrmCliente_Load(null, null);


                        }
                    }
                    //Si se esta modificando un registro
                    else
                    {
                        //Se hace lo mismo excepto lo de considerar el id
                        Cliente cliente = new Cliente();
                        cliente.Id = Convert.ToInt32(txtId.Text);
                        cliente.Nombre = txtNombre.Text;
                        cliente.Apellido = txtApellido.Text;
                        cliente.Direccion = txtDireccion.Text;
                        cliente.Telefono = txtTelefono.Text;
                        cliente.TipoDoc = Convert.ToInt32(txtTipoDoc.Text);
                        cliente.NumeroDoc = Convert.ToInt32(txtNumeroDoc.Text);

                        if (FCliente.Actualizar(cliente) > 0)
                        {
                            MessageBox.Show("Datos Modificados correctamente");
                            //Llamamos el formulario load
                            //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                            //Mostrar nuevamente actualizado los registros
                            FrmCliente_Load(null, null);


                        }


                    }//Fin del else interno
                }
                else {
                    MessageBox.Show("Faltan cargar datos: \n"+sresultado);

                }


            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }
        }//Fin boton guardar

        //Creamos un metodo o funcion para ocultar los demas botones innecesarios al realizar una operacion

        public void mostrarGuardarCancelar(Boolean b) {
            btnGuardar.Visible = b;
            btnCancelar.Visible = b;
            btnEditar.Visible = !b;
            btnEliminar.Visible = !b;
            btnNuevo.Visible = !b;
            //La grid con los registros tampoco deberia poder modificarse si se esta agregando uno nuevo o editando
            dgvClientes.Enabled = !b;

            //No puedo modificar los datos
            txtNombre.Enabled = b;
            txtApellido.Enabled = b;
            txtTipoDoc.Enabled = b;
            txtNumeroDoc.Enabled = b;
            txtDireccion.Enabled = b;
            txtTelefono.Enabled = b;
        }

        //funcion para validar datos obligatorios
        public string validarDatos() {
            string resultado="";
            if (txtNombre.Text == "") {
                resultado = resultado + "Nombre \n";
            }
            if (txtApellido.Text == "")
            {

                resultado = resultado + "Apellido \n";
            }

            return resultado;

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            //Al escoger la opcion de nuevo deberia solo ser visible el boton de guardar o cancelar la crecion del nuevo registro
            mostrarGuardarCancelar(true);

            //Limpiamos las casillas
            txtId.Text = "";
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtTipoDoc.Text = "";
            txtNumeroDoc.Text = "";
            txtTelefono.Text = "";
            txtDireccion.Text = "";

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(true);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(false);
            dgvClientes_CellClick(null, null);
        }

        //Cuando seleccionemos debemos pasar los datos del registro a los campos de texto
        private void dgvClientes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvClientes.CurrentRow != null)
            {
                txtId.Text = dgvClientes.CurrentRow.Cells[1].Value.ToString();
                txtNombre.Text = dgvClientes.CurrentRow.Cells[2].Value.ToString();
                txtApellido.Text = dgvClientes.CurrentRow.Cells[3].Value.ToString();
                txtTipoDoc.Text = dgvClientes.CurrentRow.Cells[4].Value.ToString();
                txtNumeroDoc.Text = dgvClientes.CurrentRow.Cells[5].Value.ToString();
                txtTelefono.Text = dgvClientes.CurrentRow.Cells[6].Value.ToString();
                txtDireccion.Text = dgvClientes.CurrentRow.Cells[7].Value.ToString();
            }
        }

        //Evento proveniente de la grilla
        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Verificar que el click vino de la checkbox
            if (e.ColumnIndex == dgvClientes.Columns["Eliminar"].Index) {
                DataGridViewCheckBoxCell chkEliminar=
                (DataGridViewCheckBoxCell)dgvClientes.Rows[e.RowIndex].Cells["Eliminar"];
                //Se le pone el valor opuesto seleccionado-->DesSeleccionado
                chkEliminar.Value = !Convert.ToBoolean(chkEliminar.Value);

            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                //Verificamos que realmente quiere eliminar los registros seleccionados
                if(MessageBox.Show("Realmente desea eliminar los clientes seleccionados?","Eliminacion de cliente",MessageBoxButtons.OKCancel,MessageBoxIcon.Question)==DialogResult.OK)
                {
                    //Recorrer y obtener todos los id de clientes donde este checkeado
                    foreach (DataGridViewRow row in dgvClientes.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells["Eliminar"].Value))
                        {
                            //Si esta checkeado
                            Cliente cliente = new Cliente();
                            //NOTA: Verificar que la columna si sea IdCliente---------------
                            cliente.Id = Convert.ToInt32(row.Cells["IdCliente"].Value);
                            if (FCliente.Eliminar(cliente) != 1)
                            {
                                MessageBox.Show("El cliente no pudo ser eliminado", "Eliminacion de cliente", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                            }

                        }


                    }

                    //Volvemos a cargar la lista de registros una vez se han eliminado
                    FrmCliente_Load(null, null);
                }//Fin del if
                




            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        //Evento buscar
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = new DataView(dt.Copy());
                dv.RowFilter=cmbBuscar.Text+ " Like '%"+txtBuscar.Text+"%'";
                dgvClientes.DataSource = dv;

                if (dv.Count == 0)
                {
                    lblDatosNoEncontrados.Visible = true;
                }
                else {
                    lblDatosNoEncontrados.Visible = false;

                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message + ex.StackTrace);


            }



        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        internal void setFlag(string p)
        {
            txtFlag.Text = p;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void dgvClientes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (txtFlag.Text == "1")
            {
                FrmFactura f = FrmFactura.GetInstance();
                if (dgvClientes.CurrentRow != null)
                {
                    f.setCliente(dgvClientes.CurrentRow.Cells[1].Value.ToString(), dgvClientes.CurrentRow.Cells[2].Value.ToString());
                    f.Show();
                    Close();
                }

            }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
