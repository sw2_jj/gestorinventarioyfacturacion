﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Entidades
{
    public class ProductoXFactura
    {
        //idCompra, idFactura, idProducto, cantidad
        private int _idCompra;
        private Factura _idFactura;
        private Producto _idProducto;
        private int cantidad;
        public ProductoXFactura()
        {
            _idProducto = new Producto();
            _idFactura = new Factura();
        }

        public ProductoXFactura(int idCompra,Factura factura, Producto producto, int cantidad)
        {
            IdCompra = _idCompra;
            Factura = factura;
            Producto = producto;
            Cantidad = cantidad;
        }

        public Factura Factura { get => _idFactura; set => _idFactura = value; }
        public Producto Producto { get => _idProducto; set => _idProducto = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public int IdCompra { get => _idCompra; set => _idCompra = value; }
    }
}
