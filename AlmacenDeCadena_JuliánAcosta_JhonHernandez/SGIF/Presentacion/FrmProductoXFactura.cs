﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SGIF.Entidades;
using SGIF.Datos;

namespace SGIF.Presentacion
{
    public partial class FrmProductoXFactura : Form
    {
        private static FrmProductoXFactura _instancia;
        public static DataTable dt = new DataTable();

        public static FrmProductoXFactura GetInstance()
        {

            if (_instancia == null) _instancia = new FrmProductoXFactura();

            return _instancia;

        }

        private void FrmProductoXFactura_Load(object sender, EventArgs e)
        {
            txtTotalFactura.Text = " ";
            try
            {
                DataSet ds = FProductoXFactura.GetAll(Convert.ToInt32(txtId.Text));

                dt = ds.Tables[0];
                btnImprimir.DataSource = dt;
                btnImprimir.Columns["idFactura"].Visible = false;
                btnImprimir.Columns["idProducto"].Visible = false;
                //Si hay datos no mostramos la etiqueta de "no se encontraron datos" 
                if (dt.Rows.Count > 0)
                {
                    
                    lblDatosNoEncontrados.Visible = false;
                    //dgvPF_CellClick(null, null);

                }
                else
                {
                    lblDatosNoEncontrados.Visible = true;
                }

                //mostrarGuardarCancelar(false);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);


            }
        }




        public FrmProductoXFactura()
        {
            InitializeComponent();
        }

        public void setProducto(Producto producto)

        {
            txtIdProducto.Text = producto.IdProducto.ToString();
            txtDescripcionProducto.Text = producto.DescripcionProducto;
            txtCantidadDisponible.Text = producto.CantidadDisponible.ToString();
            txtPrecioUnitario.Text = producto.Precio.ToString();
        }


        private void btnBuscarProducto_Click(object sender, EventArgs e)
        {
             FrmProducto frmP = FrmProducto.GetInstance();
            //1 significa que fue llamado desde el formulario producto
            frmP.setFlag("1");
            frmP.ShowDialog();
        }

        internal void setFactura(Factura factura)
        {
            txtId.Text = factura.Id.ToString();
            txtIdCliente.Text = factura.Cliente.Id.ToString();
            txtCliente.Text = factura.Cliente.Nombre;
        }


        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                //Validamos antes de guardar
                string sresultado = validarDatos();
                if (sresultado == "")
                {
                    //Es importante saber si el guardar procede de una modificacion o de un nuevo registro
                    if (txtId.Text.Equals(""))//Si se esta creando uno nuevo
                    {

                        ProductoXFactura pf = new ProductoXFactura();
                        pf.Factura.Id = Convert.ToInt32(txtId.Text);
                        pf.Producto.IdProducto = Convert.ToInt32(txtIdProducto.Text);
                        pf.Cantidad = Convert.ToInt32(txtCantidad.Text);

                        int i = FProductoXFactura.Insertar(pf);
                        if (i > 0)
                        {
                            FProductoXFactura.disminuirStock(pf);
                            MessageBox.Show("El producto se agrego satisfactoriamente");
                            Limpiar();
                        }
                        else {
                            MessageBox.Show("El producto no se ha agregado, intente nuevamente");
                        }
                    }
                    //Si se esta modificando un registro
                    
                }
                else
                {
                    MessageBox.Show("Faltan cargar datos: \n" + sresultado);

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }

        }

        private void Limpiar()
        {
            txtIdProducto.Text = "";
            txtDescripcionProducto.Text = "";
            txtCantidad.Text = "1";
            txtPrecioUnitario.Text = "";
            txtCantidadDisponible.Text = "0";
        }


        public string validarDatos()
        {
            string resultado = "";
            if (txtIdProducto.Text== "")
            {
                resultado = resultado + "Debe seleccionar al menos un producto \n";
            }
            if (Convert.ToInt32(txtCantidad.Text) > Convert.ToInt32(txtCantidadDisponible.Text)) {

                resultado = resultado + "la cantidad de productos a vender no puede superar los disponibles \n";
            }
    

            return resultado;

        }

        private void btnGuardar_Click_1(object sender, EventArgs e)
        {
            try
            {
                //Validamos antes de guardar
                string sresultado = validarDatos();
                if (sresultado == "")
                {
                        ProductoXFactura pf = new ProductoXFactura();
                        pf.Factura.Id = Convert.ToInt32(txtId.Text);
                        pf.Producto.IdProducto = Convert.ToInt32(txtIdProducto.Text);
                        pf.Cantidad = Convert.ToInt32(txtCantidad.Text);

                        int i = FProductoXFactura.Insertar(pf);
                    
                    if (i > 0)
                        {
                            FProductoXFactura.disminuirStock(pf);
                        FrmProductoXFactura_Load(null, null);
                        MessageBox.Show("El producto se agrego satisfactoriamente");
                            Limpiar();
                        }
                        else
                        {
                            MessageBox.Show("El producto no se ha agregado, intente nuevamente");
                        }
                  
                }
                else
                {
                    MessageBox.Show("Faltan cargar datos: \n" + sresultado);

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }
            
        }

        private void dgvPF_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Verificar que el click vino de la checkbox
            if (e.ColumnIndex == btnImprimir.Columns["Eliminar"].Index)
            {
                DataGridViewCheckBoxCell chkEliminar =
                (DataGridViewCheckBoxCell)btnImprimir.Rows[e.RowIndex].Cells["Eliminar"];
                //Se le pone el valor opuesto seleccionado-->DesSeleccionado
                chkEliminar.Value = !Convert.ToBoolean(chkEliminar.Value);

            }
        }

        private void bntQuitar1_Click(object sender, EventArgs e)
        {
            try
            {
                //Verificamos que realmente quiere eliminar los registros seleccionados
                if (MessageBox.Show("Realmente desea eliminar los productos seleccionados?", "Eliminacion de producto por factura", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    //Recorrer y obtener todos los id de clientes donde este checkeado
                    foreach (DataGridViewRow row in btnImprimir.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells["Eliminar"].Value))
                        {
                            //Si esta checkeado
                            ProductoXFactura pf = new ProductoXFactura();

                            pf.Producto.IdProducto= Convert.ToInt32(row.Cells["IdProducto"].Value);
                            pf.Factura.Id= Convert.ToInt32(row.Cells["IdFactura"].Value);
                            pf.Cantidad = Convert.ToInt32(row.Cells["cantidad"].Value);
                            
                            if (FProductoXFactura.Eliminar(pf)>0){
                                if (FProductoXFactura.aumentarStock(pf) != 1)
                                {
                                    MessageBox.Show("no se pudo actualzar el stock", "Eliminacion de producto", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                                }
                            }else{
                                MessageBox.Show("El producto no pudo ser eliminado de la venta, intente nuevamente", "Eliminacion de producto", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }


                    }

                    //Volvemos a cargar la lista de registros una vez se han eliminado
                    FrmProductoXFactura_Load(null, null);
                }//Fin del if





            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            FrmReporteFactura RP = new FrmReporteFactura();
            RP.setFacturaId(Convert.ToInt32(txtId.Text));
            RP.ShowDialog();

        }

        private void btnCalcularTotal_Click(object sender, EventArgs e)
        {
            int total = 0;

            foreach (DataGridViewRow row in btnImprimir.Rows) {
                total = total + (Convert.ToInt32(row.Cells["cantidad"].Value) * Convert.ToInt32(row.Cells["precio"].Value));
            }
            txtTotalFactura.Text = total.ToString();

        }
    }
}
