﻿using SGIF.Entidades;
using SisVenttas.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Datos
{
    class FProducto
    {
        public static DataSet GetAll()
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {

                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            return FDBHelper.ExecuteDataSet("usp_Data_FProducto_GetAll", dbParams);

        }

        //metodo Insertar

        public static int Insertar(Producto producto)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    //descripcionProducto, idTipoProducto, cantidadDisponible, precio, idProveedor
               
                    FDBHelper.MakeParam("@descripcionProducto",SqlDbType.VarChar,0,producto.DescripcionProducto),
                    FDBHelper.MakeParam("@idTipoProducto",SqlDbType.Int,0,producto.TipoProducto.Id),
                    FDBHelper.MakeParam("@cantidadDisponible",SqlDbType.Int,0,producto.CantidadDisponible),
                    FDBHelper.MakeParam("@precio",SqlDbType.Int,0,producto.Precio),
                    FDBHelper.MakeParam("@idProveedor",SqlDbType.Int,0,producto.Proveedor.Nit)
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FProducto_Insertar", dbParams));

        }

        //Metodo Actualizar
        public static int Actualizar(Producto producto)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idProducto",SqlDbType.Int,0,producto.IdProducto),
                    FDBHelper.MakeParam("@descripcionProducto",SqlDbType.VarChar,0,producto.DescripcionProducto),
                    FDBHelper.MakeParam("@idTipoProducto",SqlDbType.Int,0,producto.TipoProducto.Id),
                    FDBHelper.MakeParam("@cantidadDisponible",SqlDbType.Int,0,producto.CantidadDisponible),
                    FDBHelper.MakeParam("@precio",SqlDbType.Int,0,producto.Precio),
                    FDBHelper.MakeParam("@idProveedor",SqlDbType.Int,0,producto.Proveedor.Nit)
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos


            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FProducto_Actualizar", dbParams));

        }

        //Metodo Eliminar
        public static int Eliminar(Producto producto)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idProducto",SqlDbType.Int,0,producto.IdProducto)
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FProducto_Eliminar", dbParams));

        }
    }
}
