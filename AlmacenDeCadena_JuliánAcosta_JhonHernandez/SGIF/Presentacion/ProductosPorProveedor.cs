﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Presentacion
{
    public partial class ProductosPorProveedor : Form
    {
        public ProductosPorProveedor()
        {
            InitializeComponent();
        }

        private void ProductosPorProveedor_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'productosPorProveedor1.Productos_x_Proveedor' Puede moverla o quitarla según sea necesario.
            this.productos_x_ProveedorTableAdapter.Fill(this.productosPorProveedor1.Productos_x_Proveedor);

        }

        private void ProductosPorProveedor_Load_1(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'productosPorProveedor2.Productos_x_Proveedor' Puede moverla o quitarla según sea necesario.
            this.productos_x_ProveedorTableAdapter1.Fill(this.productosPorProveedor2.Productos_x_Proveedor);

        }
    }
}
