USE [master]
GO
/****** Object:  Database [SistemaGestorDeInventarioYFacturacion]    Script Date: 05/06/2018 11:47:56 ******/
CREATE DATABASE [SistemaGestorDeInventarioYFacturacion]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SistemaGestorDeInventarioYFacturacion', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\SistemaGestorDeInventarioYFacturacion.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SistemaGestorDeInventarioYFacturacion_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\SistemaGestorDeInventarioYFacturacion_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SistemaGestorDeInventarioYFacturacion].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET ARITHABORT OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET  MULTI_USER 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET DELAYED_DURABILITY = DISABLED 
GO
USE [SistemaGestorDeInventarioYFacturacion]
GO
/****** Object:  Table [dbo].[Auditoria]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Auditoria](
	[IdRegistro] [int] NOT NULL,
	[UsuarioWindows] [varchar](50) NULL,
	[UsuarioSQL] [varchar](50) NULL,
	[Maquina] [varchar](50) NULL,
	[FechaHora] [datetime] NULL,
	[Accion] [varchar](15) NULL,
	[NombreTabla] [varchar](50) NULL,
	[Clave] [varchar](50) NULL,
 CONSTRAINT [PK_Auditoria] PRIMARY KEY CLUSTERED 
(
	[IdRegistro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AuditoriaUpdates]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AuditoriaUpdates](
	[IdRegistro] [int] NOT NULL,
	[Campo] [varchar](50) NULL,
	[ValorAnterior] [varchar](50) NULL,
	[ValorNuevo] [varchar](50) NULL,
 CONSTRAINT [PK_AuditoriaUpdates] PRIMARY KEY CLUSTERED 
(
	[IdRegistro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cargos]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cargos](
	[idCargo] [int] IDENTITY(1,1) NOT NULL,
	[descripcionCargo] [varchar](20) NOT NULL,
	[salario] [int] NOT NULL,
 CONSTRAINT [PK_Cargos] PRIMARY KEY CLUSTERED 
(
	[idCargo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cargosXtrabajador]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cargosXtrabajador](
	[idTrabajador] [int] NOT NULL,
	[idCargo] [int] NOT NULL,
	[fechaInicio] [date] NOT NULL,
	[fechaFin] [date] NOT NULL,
 CONSTRAINT [PK_cargosXtrabajador] PRIMARY KEY CLUSTERED 
(
	[idTrabajador] ASC,
	[idCargo] ASC,
	[fechaInicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clientes](
	[IdCliente] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido] [varchar](50) NOT NULL,
	[tipoDocumento] [int] NOT NULL,
	[numeroDocumento] [int] NOT NULL,
	[telefono] [varchar](13) NULL,
	[direccion] [varchar](30) NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Factura]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Factura](
	[idFactura] [int] IDENTITY(1,1) NOT NULL,
	[idCliente] [int] NOT NULL,
	[fecha] [date] NOT NULL,
 CONSTRAINT [PK_Factura] PRIMARY KEY CLUSTERED 
(
	[idFactura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Productos]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Productos](
	[idProducto] [int] IDENTITY(1,1) NOT NULL,
	[descripcionProducto] [varchar](25) NOT NULL,
	[idTipoProducto] [int] NOT NULL,
	[cantidadDisponible] [int] NOT NULL,
	[precio] [int] NOT NULL,
	[idProveedor] [int] NOT NULL,
 CONSTRAINT [PK_Productos] PRIMARY KEY CLUSTERED 
(
	[idProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[productosXFactura]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[productosXFactura](
	[idFactura] [int] NOT NULL,
	[idProducto] [int] NOT NULL,
	[cantidad] [int] NOT NULL,
 CONSTRAINT [PK_productosXFactura] PRIMARY KEY CLUSTERED 
(
	[idFactura] ASC,
	[idProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Proveedores]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Proveedores](
	[nit] [int] NOT NULL,
	[razonSocial] [varchar](30) NOT NULL,
	[representanteLegal] [varchar](30) NOT NULL,
	[direccion] [varchar](30) NULL,
	[telefono] [varchar](14) NULL,
	[Fax] [varchar](15) NULL,
 CONSTRAINT [PK_Proveedores] PRIMARY KEY CLUSTERED 
(
	[nit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tipoDocumento]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tipoDocumento](
	[idTipoDocumento] [int] IDENTITY(1,1) NOT NULL,
	[descripcionTipoDoc] [varchar](20) NOT NULL,
 CONSTRAINT [PK_tipoDocumento] PRIMARY KEY CLUSTERED 
(
	[idTipoDocumento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tipoProducto]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tipoProducto](
	[idTipoProducto] [int] IDENTITY(1,1) NOT NULL,
	[descripcionTipoProducto] [varchar](30) NOT NULL,
 CONSTRAINT [PK_tipoProducto] PRIMARY KEY CLUSTERED 
(
	[idTipoProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Trabajadores]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Trabajadores](
	[idTrabajador] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](20) NULL,
	[apellido] [varchar](20) NULL,
	[tipoDocumento] [int] NULL,
	[numeroDoc] [int] NULL,
	[telefono] [varchar](13) NOT NULL,
	[direccion] [varchar](30) NOT NULL,
	[fechaNacimiento] [date] NULL,
 CONSTRAINT [PK_Trabajadores] PRIMARY KEY CLUSTERED 
(
	[idTrabajador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Apellido] [varchar](50) NULL,
	[DNI] [int] NULL,
	[Direccion] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NOT NULL,
	[Usuario] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[Tipo] [varchar](20) NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[Productos x Proveedor]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Productos x Proveedor]
AS
SELECT        dbo.Productos.descripcionProducto, dbo.Proveedores.razonSocial, dbo.Proveedores.representanteLegal, dbo.Proveedores.direccion, 
                         dbo.Proveedores.telefono
FROM            dbo.Productos INNER JOIN
                         dbo.Proveedores ON dbo.Productos.idProveedor = dbo.Proveedores.nit

GO
/****** Object:  View [dbo].[productosMasVendidos]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[productosMasVendidos]
AS
SELECT        TOP (100) PERCENT dbo.Productos.idProducto, dbo.Productos.descripcionProducto, dbo.Productos.precio AS PrecioUnitario, SUM(dbo.productosXFactura.cantidad) 
                         AS TotalVendido
FROM            dbo.Productos INNER JOIN
                         dbo.productosXFactura ON dbo.Productos.idProducto = dbo.productosXFactura.idProducto
GROUP BY dbo.Productos.idProducto, dbo.Productos.descripcionProducto, dbo.Productos.precio
ORDER BY TotalVendido DESC

GO
/****** Object:  View [dbo].[View_1]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_1]
AS
SELECT        dbo.Factura.idFactura, dbo.Factura.fecha, dbo.Clientes.nombre, dbo.Clientes.apellido, dbo.Clientes.numeroDocumento, dbo.Productos.descripcionProducto, dbo.productosXFactura.cantidad, dbo.Productos.precio, 
                         dbo.productosXFactura.cantidad * dbo.Productos.precio AS TotalProducto
FROM            dbo.Clientes INNER JOIN
                         dbo.Factura ON dbo.Clientes.IdCliente = dbo.Factura.idCliente INNER JOIN
                         dbo.productosXFactura ON dbo.Factura.idFactura = dbo.productosXFactura.idFactura INNER JOIN
                         dbo.Productos ON dbo.productosXFactura.idProducto = dbo.Productos.idProducto

GO
/****** Object:  View [dbo].[ViewVentasAnuales]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewVentasAnuales]
AS
SELECT        TOP (100) PERCENT YEAR(dbo.Factura.fecha) AS Año, SUM(dbo.Productos.precio * dbo.productosXFactura.cantidad) AS [Total Ventas]
FROM            dbo.Factura INNER JOIN
                         dbo.productosXFactura ON dbo.Factura.idFactura = dbo.productosXFactura.idFactura INNER JOIN
                         dbo.Productos ON dbo.productosXFactura.idProducto = dbo.Productos.idProducto
GROUP BY YEAR(dbo.Factura.fecha)
ORDER BY Año DESC

GO
SET IDENTITY_INSERT [dbo].[Cargos] ON 

INSERT [dbo].[Cargos] ([idCargo], [descripcionCargo], [salario]) VALUES (2, N'vendedor', 100000)
SET IDENTITY_INSERT [dbo].[Cargos] OFF
SET IDENTITY_INSERT [dbo].[Clientes] ON 

INSERT [dbo].[Clientes] ([IdCliente], [nombre], [apellido], [tipoDocumento], [numeroDocumento], [telefono], [direccion]) VALUES (3, N'pedro andres', N'hernadez', 4, 32424242, N'818', N'cra 767')
INSERT [dbo].[Clientes] ([IdCliente], [nombre], [apellido], [tipoDocumento], [numeroDocumento], [telefono], [direccion]) VALUES (6, N'carlos', N'medina', 1, 121232323, N'128712', N'cra 76 - 67')
INSERT [dbo].[Clientes] ([IdCliente], [nombre], [apellido], [tipoDocumento], [numeroDocumento], [telefono], [direccion]) VALUES (7, N'andres', N'velez', 2, 192912, N'312441', N'cra 44')
SET IDENTITY_INSERT [dbo].[Clientes] OFF
SET IDENTITY_INSERT [dbo].[Factura] ON 

INSERT [dbo].[Factura] ([idFactura], [idCliente], [fecha]) VALUES (1, 4, CAST(N'2018-06-04' AS Date))
INSERT [dbo].[Factura] ([idFactura], [idCliente], [fecha]) VALUES (2, 3, CAST(N'2018-06-04' AS Date))
INSERT [dbo].[Factura] ([idFactura], [idCliente], [fecha]) VALUES (3, 4, CAST(N'2018-06-04' AS Date))
INSERT [dbo].[Factura] ([idFactura], [idCliente], [fecha]) VALUES (4, 4, CAST(N'2018-06-04' AS Date))
INSERT [dbo].[Factura] ([idFactura], [idCliente], [fecha]) VALUES (5, 4, CAST(N'2018-06-04' AS Date))
INSERT [dbo].[Factura] ([idFactura], [idCliente], [fecha]) VALUES (6, 4, CAST(N'2018-06-04' AS Date))
INSERT [dbo].[Factura] ([idFactura], [idCliente], [fecha]) VALUES (7, 5, CAST(N'2018-06-04' AS Date))
INSERT [dbo].[Factura] ([idFactura], [idCliente], [fecha]) VALUES (8, 7, CAST(N'2018-06-05' AS Date))
INSERT [dbo].[Factura] ([idFactura], [idCliente], [fecha]) VALUES (9, 6, CAST(N'2018-06-05' AS Date))
INSERT [dbo].[Factura] ([idFactura], [idCliente], [fecha]) VALUES (10, 6, CAST(N'2018-06-05' AS Date))
INSERT [dbo].[Factura] ([idFactura], [idCliente], [fecha]) VALUES (11, 7, CAST(N'2018-06-05' AS Date))
SET IDENTITY_INSERT [dbo].[Factura] OFF
SET IDENTITY_INSERT [dbo].[Productos] ON 

INSERT [dbo].[Productos] ([idProducto], [descripcionProducto], [idTipoProducto], [cantidadDisponible], [precio], [idProveedor]) VALUES (1, N'leche', 1, 0, 1000, 12)
INSERT [dbo].[Productos] ([idProducto], [descripcionProducto], [idTipoProducto], [cantidadDisponible], [precio], [idProveedor]) VALUES (2, N'yogurt', 1, 0, 600, 12)
INSERT [dbo].[Productos] ([idProducto], [descripcionProducto], [idTipoProducto], [cantidadDisponible], [precio], [idProveedor]) VALUES (3, N'queso', 1, 39, 5000, 12)
INSERT [dbo].[Productos] ([idProducto], [descripcionProducto], [idTipoProducto], [cantidadDisponible], [precio], [idProveedor]) VALUES (4, N'queso', 1, 116, 1000, 21)
SET IDENTITY_INSERT [dbo].[Productos] OFF
INSERT [dbo].[productosXFactura] ([idFactura], [idProducto], [cantidad]) VALUES (1, 1, 2)
INSERT [dbo].[productosXFactura] ([idFactura], [idProducto], [cantidad]) VALUES (1, 2, 7)
INSERT [dbo].[productosXFactura] ([idFactura], [idProducto], [cantidad]) VALUES (3, 1, 9)
INSERT [dbo].[productosXFactura] ([idFactura], [idProducto], [cantidad]) VALUES (3, 2, 1)
INSERT [dbo].[productosXFactura] ([idFactura], [idProducto], [cantidad]) VALUES (4, 2, 4)
INSERT [dbo].[productosXFactura] ([idFactura], [idProducto], [cantidad]) VALUES (6, 2, 7)
INSERT [dbo].[productosXFactura] ([idFactura], [idProducto], [cantidad]) VALUES (6, 3, 3)
INSERT [dbo].[productosXFactura] ([idFactura], [idProducto], [cantidad]) VALUES (7, 3, 2)
INSERT [dbo].[productosXFactura] ([idFactura], [idProducto], [cantidad]) VALUES (8, 2, 20)
INSERT [dbo].[productosXFactura] ([idFactura], [idProducto], [cantidad]) VALUES (9, 2, 185)
INSERT [dbo].[productosXFactura] ([idFactura], [idProducto], [cantidad]) VALUES (9, 3, 1)
INSERT [dbo].[productosXFactura] ([idFactura], [idProducto], [cantidad]) VALUES (11, 3, 5)
INSERT [dbo].[productosXFactura] ([idFactura], [idProducto], [cantidad]) VALUES (11, 4, 7)
INSERT [dbo].[Proveedores] ([nit], [razonSocial], [representanteLegal], [direccion], [telefono], [Fax]) VALUES (12, N'PGK', N'carlos', N'cra', N'1', N'1')
INSERT [dbo].[Proveedores] ([nit], [razonSocial], [representanteLegal], [direccion], [telefono], [Fax]) VALUES (21, N'Colanta', N'pedro humbero', N'cra 67', N'71616', N'1414-33')
INSERT [dbo].[Proveedores] ([nit], [razonSocial], [representanteLegal], [direccion], [telefono], [Fax]) VALUES (122, N'colanta2', N'luis', N'cr', N'121', N'1212')
INSERT [dbo].[Proveedores] ([nit], [razonSocial], [representanteLegal], [direccion], [telefono], [Fax]) VALUES (123, N'nutressa', N'carlos enrique', N'cra 76  - 77 88', N'324424', N'4144141-3')
SET IDENTITY_INSERT [dbo].[tipoDocumento] ON 

INSERT [dbo].[tipoDocumento] ([idTipoDocumento], [descripcionTipoDoc]) VALUES (1, N'CC')
INSERT [dbo].[tipoDocumento] ([idTipoDocumento], [descripcionTipoDoc]) VALUES (2, N'TI')
INSERT [dbo].[tipoDocumento] ([idTipoDocumento], [descripcionTipoDoc]) VALUES (3, N'CE')
INSERT [dbo].[tipoDocumento] ([idTipoDocumento], [descripcionTipoDoc]) VALUES (4, N'PP')
SET IDENTITY_INSERT [dbo].[tipoDocumento] OFF
SET IDENTITY_INSERT [dbo].[tipoProducto] ON 

INSERT [dbo].[tipoProducto] ([idTipoProducto], [descripcionTipoProducto]) VALUES (1, N'Lacteos')
INSERT [dbo].[tipoProducto] ([idTipoProducto], [descripcionTipoProducto]) VALUES (2, N'aseo')
SET IDENTITY_INSERT [dbo].[tipoProducto] OFF
SET IDENTITY_INSERT [dbo].[Trabajadores] ON 

INSERT [dbo].[Trabajadores] ([idTrabajador], [nombre], [apellido], [tipoDocumento], [numeroDoc], [telefono], [direccion], [fechaNacimiento]) VALUES (4, N'carlos', N'calle', 1, 10109292, N'6161616', N'cra 57', CAST(N'2018-02-07' AS Date))
INSERT [dbo].[Trabajadores] ([idTrabajador], [nombre], [apellido], [tipoDocumento], [numeroDoc], [telefono], [direccion], [fechaNacimiento]) VALUES (5, N'sdsd', N'dsd', 1, 2121, N'1212', N'12112', CAST(N'2018-01-31' AS Date))
SET IDENTITY_INSERT [dbo].[Trabajadores] OFF
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

INSERT [dbo].[Usuarios] ([id], [Nombre], [Apellido], [DNI], [Direccion], [Telefono], [Usuario], [Password], [Tipo]) VALUES (1, N'john', N'hernandez', 10399329, N'cr45', N'3193930330', N'admin', N'admin', N'Administracion')
INSERT [dbo].[Usuarios] ([id], [Nombre], [Apellido], [DNI], [Direccion], [Telefono], [Usuario], [Password], [Tipo]) VALUES (2, N'maria', N'Acosta', 19329320, N'cr67', N'3193993029', N'maria1', N'maria1', N'Venta')
INSERT [dbo].[Usuarios] ([id], [Nombre], [Apellido], [DNI], [Direccion], [Telefono], [Usuario], [Password], [Tipo]) VALUES (6, N'julian', N'Acosta', 19394299, N'cr43', N'3142346543', N'ju', N'julio', N'Almacen')
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
ALTER TABLE [dbo].[cargosXtrabajador]  WITH CHECK ADD  CONSTRAINT [FK_cargosXtrabajador_Cargos] FOREIGN KEY([idCargo])
REFERENCES [dbo].[Cargos] ([idCargo])
GO
ALTER TABLE [dbo].[cargosXtrabajador] CHECK CONSTRAINT [FK_cargosXtrabajador_Cargos]
GO
ALTER TABLE [dbo].[cargosXtrabajador]  WITH CHECK ADD  CONSTRAINT [FK_cargosXtrabajador_Trabajadores] FOREIGN KEY([idTrabajador])
REFERENCES [dbo].[Trabajadores] ([idTrabajador])
GO
ALTER TABLE [dbo].[cargosXtrabajador] CHECK CONSTRAINT [FK_cargosXtrabajador_Trabajadores]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK_tipoDocumento] FOREIGN KEY([tipoDocumento])
REFERENCES [dbo].[tipoDocumento] ([idTipoDocumento])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK_tipoDocumento]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_idProveedor] FOREIGN KEY([idProveedor])
REFERENCES [dbo].[Proveedores] ([nit])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_idProveedor]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_tipoProducto_IdTipoProducto] FOREIGN KEY([idTipoProducto])
REFERENCES [dbo].[tipoProducto] ([idTipoProducto])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_tipoProducto_IdTipoProducto]
GO
ALTER TABLE [dbo].[productosXFactura]  WITH CHECK ADD  CONSTRAINT [FK_PxF___idFactura] FOREIGN KEY([idFactura])
REFERENCES [dbo].[Factura] ([idFactura])
GO
ALTER TABLE [dbo].[productosXFactura] CHECK CONSTRAINT [FK_PxF___idFactura]
GO
ALTER TABLE [dbo].[productosXFactura]  WITH CHECK ADD  CONSTRAINT [FK_PxF___idProducto] FOREIGN KEY([idProducto])
REFERENCES [dbo].[Productos] ([idProducto])
GO
ALTER TABLE [dbo].[productosXFactura] CHECK CONSTRAINT [FK_PxF___idProducto]
GO
ALTER TABLE [dbo].[Trabajadores]  WITH CHECK ADD  CONSTRAINT [FK_idTipoDoc_Trabajadores] FOREIGN KEY([tipoDocumento])
REFERENCES [dbo].[tipoDocumento] ([idTipoDocumento])
GO
ALTER TABLE [dbo].[Trabajadores] CHECK CONSTRAINT [FK_idTipoDoc_Trabajadores]
GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FCargo_Actualizar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FCargo_Actualizar]
	-- lista de parametros
	(
	@idCargo int,	
	@descripcionCargo varchar(20),
	@salario int
	)

AS
update Cargos set descripcionCargo=@descripcionCargo,salario=@salario
					where idCargo=@idCargo

--Ultimo valor insertado
select @@ROWCOUNT as cantidad


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FCargo_Eliminar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FCargo_Eliminar]
@idCargo int
AS
BEGIN
delete from Cargos where idCargo=@idCargo
select @@ROWCOUNT as cantidadAfectada
END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FCargo_GetAll]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Data_FCargo_GetAll]


AS
BEGIN
SELECT Cargos.idCargo, Cargos.descripcionCargo, Cargos.salario from Cargos

END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FCargo_Insertar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_Data_FCargo_Insertar]
	-- lista de parametros
	(
	@descripcionCargo varchar(20),
	@salario int
	)

AS
insert into Cargos(descripcionCargo,salario) values (@descripcionCargo,@salario)

--Ultimo valor insertado
select @@IDENTITY


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FCliente_Actualizar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FCliente_Actualizar]
	-- lista de parametros
	(
	@idCliente int,	
	@nombre varchar(50),
	@apellido varchar(50),
	@tipoDocumento int,
	@numeroDocumento int,
	@telefono varchar(13),
	@direccion varchar(30)
	)

AS
update Clientes set nombre=@nombre,apellido=@apellido,tipoDocumento=@tipoDocumento,numeroDocumento=@numeroDocumento,
                    telefono=@telefono,direccion=@direccion
					where IdCliente=@idCliente

--Ultimo valor insertado
select @@ROWCOUNT as cantidad


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FCliente_Eliminar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FCliente_Eliminar]
@id int
AS
BEGIN
delete from Clientes where IdCliente=@id
select @@ROWCOUNT as cantidadAfectada
END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FCliente_GetAll]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Data_FCliente_GetAll]


AS
BEGIN
SELECT Clientes.IdCliente,Clientes.nombre,Clientes.apellido,Clientes.tipoDocumento,Clientes.numeroDocumento,Clientes.telefono,Clientes.direccion from Clientes

END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FCliente_Insertar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FCliente_Insertar]
	-- lista de parametros
	(
	@nombre varchar(50),
	@apellido varchar(50),
	@tipoDocumento int,
	@numeroDocumento int,
	@telefono varchar(13),
	@direccion varchar(30)
	)

AS
insert into Clientes (nombre,apellido,tipoDocumento,numeroDocumento,telefono,direccion)
values(@nombre,@apellido,@tipoDocumento,@numeroDocumento,@telefono,@direccion)

--Ultimo valor insertado
select @@IDENTITY


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FFactura_Actualizar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_Data_FFactura_Actualizar]
	-- lista de parametros
	(
	@idFactura int, @idCliente int, @fecha date
	)

AS
update Factura set idCliente=@idCliente,fecha=@fecha
					where idFactura=@idFactura

--Ultimo valor insertado
select @@ROWCOUNT as cantidad


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FFactura_Eliminar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_Data_FFactura_Eliminar]
@idFactura int
AS
BEGIN
delete from Factura where idFactura=@idFactura
select @@ROWCOUNT as cantidadAfectada
END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FFactura_GetAll]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_Data_FFactura_GetAll]


AS
BEGIN
SELECT        dbo.Factura.idFactura, dbo.Factura.idCliente, dbo.Factura.fecha, dbo.Clientes.nombre, dbo.Clientes.apellido
FROM            dbo.Clientes INNER JOIN
                         dbo.Factura ON dbo.Clientes.IdCliente = dbo.Factura.idCliente
END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FFactura_Insertar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_Data_FFactura_Insertar]
	-- lista de parametros
	(
	 @idCliente int, @fecha date
	)

AS
insert into Factura(idCliente, fecha) values (@idCliente, @fecha)

--Ultimo valor insertado
select @@IDENTITY as ventaId


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FLogin_ValidarLogin]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Data_FLogin_ValidarLogin] 
@usuario varchar(50),
@password varchar(50)
AS
BEGIN

select * from dbo.Usuarios where Usuarios.Usuario=@usuario and Usuarios.Password=@password;

END

GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProducto_Actualizar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FProducto_Actualizar]
	--  descripcionProducto, idTipoProducto, cantidadDisponible, precio, idProveedor
	(
	@idProducto int,
	@descripcionProducto varchar(20),
	@idTipoProducto int,
	@cantidadDisponible int,
	@precio int,
	@idProveedor int
	)

AS
update Productos set descripcionProducto=@descripcionProducto, idTipoProducto=@idTipoProducto, cantidadDisponible=@cantidadDisponible, precio=@precio, idProveedor=@idProveedor
where idProducto=@idProducto

--Ultimo valor insertado
select @@ROWCOUNT as cantidad


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProducto_Eliminar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_Data_FProducto_Eliminar]
@idProducto int
AS
BEGIN
delete from Productos where idProducto=@idProducto
select @@ROWCOUNT as cantidadAfectada
END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProducto_GetAll]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_Data_FProducto_GetAll]

AS
BEGIN
SELECT        dbo.Productos.idProducto, dbo.Productos.descripcionProducto, dbo.Productos.idTipoProducto, dbo.tipoProducto.descripcionTipoProducto, 
                         dbo.Productos.cantidadDisponible, dbo.Productos.precio, dbo.Productos.idProveedor, dbo.Proveedores.razonSocial
FROM            dbo.Productos INNER JOIN
                         dbo.Proveedores ON dbo.Productos.idProveedor = dbo.Proveedores.nit INNER JOIN
                         dbo.tipoProducto ON dbo.Productos.idTipoProducto = dbo.tipoProducto.idTipoProducto

END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProducto_Insertar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[usp_Data_FProducto_Insertar]
	--  descripcionProducto, idTipoProducto, cantidadDisponible, precio, idProveedor
	(
	@descripcionProducto varchar(20),
	@idTipoProducto int,
	@cantidadDisponible int,
	@precio int,
	@idProveedor int
	)

AS
insert into Productos(descripcionProducto, idTipoProducto, cantidadDisponible, precio, idProveedor) values (@descripcionProducto,@idTipoProducto, @cantidadDisponible, @precio,@idProveedor)

--Ultimo valor insertado
select @@IDENTITY


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProductoXFactura_Actualizar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[usp_Data_FProductoXFactura_Actualizar]
	-- lista de parametros
	(
	@idFactura int, @idProducto int, @cantidad int
	)

AS
update productosXFactura set cantidad=@cantidad
					where idFactura=@idFactura and idProducto=@idProducto

--Ultimo valor insertado
select @@ROWCOUNT as cantidad


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProductoXFactura_AumentarStock]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FProductoXFactura_AumentarStock]
@idProducto int,
@cantidad int
AS
BEGIN
update Productos set cantidadDisponible=cantidadDisponible+@cantidad
where idProducto=@idProducto

select @@ROWCOUNT
END

GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProductoXFactura_disminuirStock]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FProductoXFactura_disminuirStock]
@idProducto int,
@cantidad int
AS
BEGIN
update Productos set cantidadDisponible=cantidadDisponible-@cantidad
where idProducto=@idProducto
select @@ROWCOUNT

END

GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProductoXFactura_Eliminar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[usp_Data_FProductoXFactura_Eliminar]
@idFactura int, @idProducto int
AS
BEGIN
delete from productosXFactura where idFactura=@idFactura and idProducto=@idProducto
select @@ROWCOUNT as cantidadAfectada
END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProductoXFactura_GetAll]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FProductoXFactura_GetAll]
@idFactura int

AS
BEGIN
SELECT        dbo.productosXFactura.idFactura, dbo.productosXFactura.idProducto, dbo.productosXFactura.cantidad, dbo.Factura.fecha, dbo.Productos.precio, 
                         dbo.Productos.descripcionProducto, dbo.Productos.idTipoProducto
FROM            dbo.Factura INNER JOIN
                         dbo.productosXFactura ON dbo.Factura.idFactura = dbo.productosXFactura.idFactura INNER JOIN
                         dbo.Productos ON dbo.productosXFactura.idProducto = dbo.Productos.idProducto
WHERE  productosXFactura.idFactura=@idFactura

END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProductoXFactura_Insertar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_Data_FProductoXFactura_Insertar]
	-- lista de parametros
	(
	@idFactura int, @idProducto int, 
	@cantidad int
	
	)

AS
insert into productosXFactura(idFactura,idProducto,cantidad) values (@idFactura,@idProducto,@cantidad)

--Ultimo valor insertado
select @@IDENTITY as ventaDetalleID


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProveedor_Actualizar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Data_FProveedor_Actualizar]
	-- lista de parametros
	(
	@nit int,
	@razonSocial varchar(50),
	@representanteLegal varchar(30),
	@direccion varchar(30),
	@telefono varchar(14),
	@Fax varchar(15)
	)

AS
update Proveedores set razonSocial=@razonSocial, representanteLegal=@representanteLegal, direccion=@direccion, telefono=@telefono, Fax=@Fax
					where nit=@nit

--Ultimo valor insertado
select @@ROWCOUNT as cantidad


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProveedor_Eliminar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_Data_FProveedor_Eliminar]
@nit int
AS
BEGIN
delete from Proveedores where nit=@nit
select @@ROWCOUNT as cantidadAfectada
END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProveedor_GetAll]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Data_FProveedor_GetAll]


AS
BEGIN
SELECT Proveedores.nit,Proveedores.razonSocial,Proveedores.representanteLegal,Proveedores.direccion,Proveedores.telefono,Proveedores.Fax from Proveedores

END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FProveedor_Insertar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FProveedor_Insertar]
	-- lista de parametros
	(
	@nit int,
	@razonSocial varchar(50),
	@representanteLegal varchar(30),
	@direccion varchar(30),
	@telefono varchar(14),
	@Fax varchar(15)
	)

AS
insert into Proveedores(nit,razonSocial,representanteLegal,direccion,telefono,Fax)
values(@nit,@razonSocial,@representanteLegal,@direccion,@telefono,@Fax)

--Ultimo valor insertado
select @@ROWCOUNT as cantidad


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FTipoDocumento_Actualizar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_Data_FTipoDocumento_Actualizar]
	-- lista de parametros
	(
	@idTipoDocumento int,	
	@descripcionTipoDoc varchar(50)
	)

AS
update tipoDocumento set descripcionTipoDoc=@descripcionTipoDoc
					where idTipoDocumento=@idTipoDocumento

--Ultimo valor insertado
select @@ROWCOUNT as cantidad


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FTipoDocumento_Eliminar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_Data_FTipoDocumento_Eliminar]
@idTipoDocumento int
AS
BEGIN
delete from tipoDocumento where idTipoDocumento=@idTipoDocumento
select @@ROWCOUNT as cantidadAfectada
END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FTipoDocumento_GetAll]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[usp_Data_FTipoDocumento_GetAll]


AS
BEGIN
SELECT tipoDocumento.idTipoDocumento,tipoDocumento.descripcionTipoDoc from tipoDocumento

END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FTipoDocumento_Insertar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_Data_FTipoDocumento_Insertar]
	-- lista de parametros
	(
	@descripcionTipoDoc varchar(30)
	)

AS
insert into tipoDocumento(descripcionTipoDoc) values (@descripcionTipoDoc)

--Ultimo valor insertado
select @@IDENTITY


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FTipoProducto_Actualizar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FTipoProducto_Actualizar]
	-- lista de parametros
	(
	@idTipoProducto int,	
	@descripcionTipoProducto varchar(50)
	)

AS
update tipoProducto set descripcionTipoProducto=@descripcionTipoProducto
					where idTipoProducto=@idTipoProducto

--Ultimo valor insertado
select @@ROWCOUNT as cantidad


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FTipoProducto_Eliminar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FTipoProducto_Eliminar]
@id int
AS
BEGIN
delete from tipoProducto where idTipoProducto=@id
select @@ROWCOUNT as cantidadAfectada
END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FTipoProducto_GetAll]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Data_FTipoProducto_GetAll]


AS
BEGIN
SELECT tipoProducto.idTipoProducto,tipoProducto.descripcionTipoProducto from tipoProducto

END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FTipoProducto_Insertar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FTipoProducto_Insertar]
	-- lista de parametros
	(
	@descripcionTipoProducto varchar(30)
	)

AS
insert into tipoProducto (descripcionTipoProducto) values (@descripcionTipoProducto)

--Ultimo valor insertado

select 1


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FTrabajador_Actualizar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_Data_FTrabajador_Actualizar]
	-- lista de parametros
	(
	@idTrabajador int,
	@nombre varchar(20),
	@apellido varchar(20),
	@tipoDocumento int,
	@numeroDocumento int,
	@telefono varchar(13),
	@direccion varchar(30),
	@fechaNacimiento date
	)

AS
update Trabajadores set nombre=@nombre,apellido=@apellido,tipoDocumento=@tipoDocumento,numeroDoc=@numeroDocumento,
                    telefono=@telefono,direccion=@direccion,fechaNacimiento=@fechaNacimiento
					where idTrabajador=@idTrabajador

--Ultimo valor insertado
select @@ROWCOUNT as cantidad


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FTrabajador_Eliminar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_Data_FTrabajador_Eliminar]
@id int
AS
BEGIN
delete from Trabajadores where idTrabajador=@id
select @@ROWCOUNT as cantidadAfectada
END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FTrabajador_GetAll]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[usp_Data_FTrabajador_GetAll]


AS
BEGIN
SELECT Trabajadores.idTrabajador,Trabajadores.nombre,Trabajadores.apellido,Trabajadores.tipoDocumento,Trabajadores.numeroDoc,Trabajadores.telefono,Trabajadores.direccion,Trabajadores.fechaNacimiento
FROM Trabajadores 
END


GO
/****** Object:  StoredProcedure [dbo].[usp_Data_FTrabajador_Insertar]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Data_FTrabajador_Insertar]
	-- lista de parametros
	(
	@nombre varchar(20),
	@apellido varchar(20),
	@tipoDocumento int,
	@numeroDocumento int,
	@telefono varchar(13),
	@direccion varchar(30),
	@fechaNacimiento date
	)

AS
insert into Trabajadores(nombre,apellido,tipoDocumento,numeroDoc,telefono,direccion,fechaNacimiento)
values(@nombre,@apellido,@tipoDocumento,@numeroDocumento,@telefono,@direccion,@fechaNacimiento)

--Ultimo valor insertado
select @@IDENTITY


GO
/****** Object:  StoredProcedure [dbo].[usp_Reportes_GenerarReporteFactura]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Reportes_GenerarReporteFactura]
@idFactura int
AS
BEGIN

 SELECT        dbo.Factura.idFactura, dbo.Factura.fecha, dbo.Clientes.nombre, dbo.Clientes.apellido, dbo.Clientes.numeroDocumento, dbo.Productos.descripcionProducto, dbo.productosXFactura.cantidad, dbo.Productos.precio, 
                         dbo.productosXFactura.cantidad * dbo.Productos.precio AS TotalProducto
 FROM            dbo.Clientes INNER JOIN
                         dbo.Factura ON dbo.Clientes.IdCliente = dbo.Factura.idCliente INNER JOIN
                         dbo.productosXFactura ON dbo.Factura.idFactura = dbo.productosXFactura.idFactura INNER JOIN
                         dbo.Productos ON dbo.productosXFactura.idProducto = dbo.Productos.idProducto
  WHERE            dbo.Factura.idFactura=@idFactura
END

GO
/****** Object:  StoredProcedure [dbo].[usp_Reportes_ProductosMasVendidos]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- =============================================
CREATE PROCEDURE [dbo].[usp_Reportes_ProductosMasVendidos]
@numero int
AS
BEGIN
select Productos.idProducto,Productos.descripcionProducto,Productos.precio as PrecioUnitario,SUM(productosXFactura.cantidad) as TotalVendido
from Productos inner join productosXFactura ON Productos.idProducto= productosXFactura.idProducto
group by Productos.idProducto,Productos.descripcionProducto,Productos.precio
order by SUM(productosXFactura.cantidad) desc
END

GO
/****** Object:  StoredProcedure [dbo].[usp_Reportes_ProductosMayorIngreso]    Script Date: 05/06/2018 11:47:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- =============================================
CREATE PROCEDURE [dbo].[usp_Reportes_ProductosMayorIngreso]
AS
BEGIN
select Productos.idProducto,Productos.descripcionProducto,SUM(Productos.precio*productosXFactura.cantidad) AS totalVentas
from Productos inner join productosXFactura ON Productos.idProducto= productosXFactura.idProducto
group by Productos.idProducto,Productos.descripcionProducto
order by SUM(Productos.precio*productosXFactura.cantidad) desc
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Productos"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Proveedores"
            Begin Extent = 
               Top = 6
               Left = 285
               Bottom = 135
               Right = 494
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Productos x Proveedor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Productos x Proveedor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Productos"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 263
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "productosXFactura"
            Begin Extent = 
               Top = 6
               Left = 301
               Bottom = 118
               Right = 526
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'productosMasVendidos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'productosMasVendidos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[27] 4[16] 2[22] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -782
      End
      Begin Tables = 
         Begin Table = "Clientes"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 232
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "Factura"
            Begin Extent = 
               Top = 6
               Left = 270
               Bottom = 119
               Right = 440
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Productos"
            Begin Extent = 
               Top = 6
               Left = 478
               Bottom = 186
               Right = 677
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "productosXFactura"
            Begin Extent = 
               Top = 136
               Left = 272
               Bottom = 260
               Right = 442
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Factura"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 118
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Productos"
            Begin Extent = 
               Top = 6
               Left = 285
               Bottom = 135
               Right = 494
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "productosXFactura"
            Begin Extent = 
               Top = 6
               Left = 532
               Bottom = 118
               Right = 741
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewVentasAnuales'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewVentasAnuales'
GO
USE [master]
GO
ALTER DATABASE [SistemaGestorDeInventarioYFacturacion] SET  READ_WRITE 
GO
