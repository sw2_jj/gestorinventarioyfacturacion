﻿using SGIF.Entidades;
using SisVenttas.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Datos
{
    public class FTipoProducto
    {
        public static DataSet GetAll()
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {

                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            return FDBHelper.ExecuteDataSet("usp_Data_FTipoProducto_GetAll", dbParams);

        }

        //metodo Insertar

        public static int Insertar(TipoProducto tipoProducto)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@descripcionTipoProducto",SqlDbType.VarChar,0,tipoProducto.TipoProduc),
                   };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FTipoProducto_Insertar", dbParams));

        }

        //Metodo Actualizar
        public static int Actualizar(TipoProducto tipoProducto )
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idTipoProducto",SqlDbType.Int,0,tipoProducto.Id),
                    FDBHelper.MakeParam("@descripcionTipoProducto",SqlDbType.VarChar,0,tipoProducto.TipoProduc),
                   };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos


            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FTipoProducto_Actualizar", dbParams));

        }

        //Metodo Eliminar
        public static int Eliminar(TipoProducto tipoProducto)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@id",SqlDbType.Int,0,tipoProducto.Id),
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FTipoProducto_Eliminar", dbParams));

        }


    }
}

