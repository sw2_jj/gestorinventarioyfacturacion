﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Clase string como tal
namespace SGIF.Entidades
{
    public class Cliente
    {
        private int _id;
        private string _nombre;
        private string _apellido;
        private int _tipoDoc;
        private int _numeroDoc;
        private string _telefono;
        private string _direccion;
        //Metodos getters and Setters
        public int Id {
            get { return _id; }
            set { _id = value; }
        }

        public int TipoDoc
        {
            get { return _tipoDoc; }
            set { _tipoDoc = value; }
        }

        public int NumeroDoc
        {
            get { return _numeroDoc; }
            set { _numeroDoc = value; }
        }

        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        public string Apellido
        {
            get { return _apellido; }
            set { _apellido = value; }
        }
        public string Telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }

        public string Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }
        //Constructor vacio
        public Cliente() { }

        //Contructor inicializando atributos
        public Cliente(int id,int tipoDoc,int numeroDoc,string nombre,string apellido,string telefono,string direccion)
        {
            this._id = id;
            this._numeroDoc = numeroDoc;
            this._tipoDoc = tipoDoc;
            this._nombre = nombre;
            this._apellido = apellido;
            this._telefono = telefono;
            this._direccion = direccion;
        }






    }
}
