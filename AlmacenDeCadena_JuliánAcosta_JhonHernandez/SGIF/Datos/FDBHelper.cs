﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace SisVenttas.Datos
{
    public class FDBHelper
    {
        public FDBHelper()
        {
          
        }
        
        public static DataSet ExecuteDataSet(string sqlSpName, SqlParameter[] dbParams)
        {
            DataSet ds = null;
            //try
            //{
                //se crea una instancia de la variable ds de tipo Dataset
                ds = new DataSet();
            //No es necesario ingresar el configutation manager pero es mas estable porque puede cambiar la direccion

                //se crea una instancia de la clase sqlConnection la cual representa la conexión abierta a la BD
                SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString"));
            //Se crea una instancia de la clase SqlCommand la cuan representa un procedimiento almacenada o una instruccion transacted-sql
            //le pasamosel parametros que recibe este método y la instancia de la conexión anterior
            SqlCommand cmd = new SqlCommand(sqlSpName, cn);
            // se accede a un método dentro de la clase sqlCommand el cual nos permite asignar un tiempo de entrada 
				cmd.CommandTimeout = 600;
                
            //accedemos a un método de la clase SqlCommand que nos permite acceder al procedimiento para un procedimiento almacenado
                cmd.CommandType = CommandType.StoredProcedure;
            //Se crea una instancia de la clase SqlDateAdapter la cual nos permite representar un conjunto de comandos de datos y conexiones
            // usados para rellenar un DataSet y actualizar la BD enviando la variable de instancia del SqlCommand
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //verificamos que el parametro que recibe este método sea diferente de nulo
            if (dbParams != null)
                {
                //inicializamos una variable de tipo SqlParameter para recorrer todo el vector que recibe el método actual
                    foreach (SqlParameter dbParam in dbParams)
                    {
                    //accedemos a un metodo de la clase SqlDataAdapter para agregar desde el primer hasta el ultimo 
                    //parametro del foreach
                        da.SelectCommand.Parameters.Add(dbParam);
                    }
                }
            //agregamos la variable ds al DataSet
                da.Fill(ds);
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            return ds;
        }



       

        public static object ExecuteScalar(string sqlSpName, SqlParameter[] dbParams)
        {
            object retVal = null;
            SqlConnection cn = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString"));
            SqlCommand cmd = new SqlCommand(sqlSpName, cn);
            cmd.CommandTimeout = Convert.ToInt16(ConfigurationManager.AppSettings.Get("connectionCommandTimeout"));
            cmd.CommandType = CommandType.StoredProcedure;

            if (dbParams != null)
            {
                foreach (SqlParameter dbParam in dbParams)
                {
                    cmd.Parameters.Add(dbParam);
                }
            }

            cn.Open();

            try
            {
                retVal = cmd.ExecuteScalar();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (null != cn)
                    cn.Close();
            }

            return retVal;
        }

        public static SqlParameter MakeParam(string paramName, SqlDbType dbType, int size, object objValue)
        {
            SqlParameter param;

            if (size > 0)
                param = new SqlParameter(paramName, dbType, size);
            else
                param = new SqlParameter(paramName, dbType);

            param.Value = objValue;

            return param;
        }

        public static SqlParameter MakeParamOutput(string paramName, SqlDbType dbType, int size)
        {
            SqlParameter param;

            if (size > 0)
                param = new SqlParameter(paramName, dbType, size);
            else
                param = new SqlParameter(paramName, dbType);

            param.Direction = ParameterDirection.Output;

            return param;
        }

        

        
    }
}
