﻿using SGIF.Entidades;
using SisVenttas.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Datos
{
    public class FTipoDocumento
    {
        public static DataSet GetAll()
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {

                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            return FDBHelper.ExecuteDataSet("usp_Data_FTipoDocumento_GetAll", dbParams);

        }

        //metodo Insertar

        public static int Insertar(TipoDoc tipoDoc)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@descripcionTipoDoc",SqlDbType.VarChar,0,tipoDoc.TipoDocu),
                   };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FTipoDocumento_Insertar", dbParams));

        }

        //Metodo Actualizar
        public static int Actualizar(TipoDoc tipoDoc)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idTipoDocumento",SqlDbType.Int,0,tipoDoc.Id),
                    FDBHelper.MakeParam("@descripcionTipoDoc",SqlDbType.VarChar,0,tipoDoc.TipoDocu),
                   };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos


            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FTipoDocumento_Actualizar", dbParams));

        }

        //Metodo Eliminar
        public static int Eliminar(TipoDoc tipoDoc)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idTipoDocumento",SqlDbType.Int,0,tipoDoc.Id),
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FTipoDocumento_Eliminar", dbParams));

        }
    }
}
