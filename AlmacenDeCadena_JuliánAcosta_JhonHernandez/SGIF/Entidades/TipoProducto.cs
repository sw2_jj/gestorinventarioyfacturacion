﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Entidades
{
    public class TipoProducto
    {
        private int _id;
        private String _descTipoProducto;

        public TipoProducto() { }

        public TipoProducto(int id, string descTipoProducto)
        {
            _id = id;
            _descTipoProducto = descTipoProducto;
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public String TipoProduc
        {
            get { return _descTipoProducto; }
            set { _descTipoProducto = value; }
        }




    }
}
