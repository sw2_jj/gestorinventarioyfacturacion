﻿using SGIF.Datos;
using SGIF.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Presentacion
{
    public partial class FrmProducto : Form
    {
        private static DataTable dt = new DataTable();
        private static FrmProducto _instancia;
        private static string x1="", y1="", x2 = "", y2 = "";

        public static FrmProducto GetInstance() {

            if (_instancia == null) _instancia = new FrmProducto();
            
            return _instancia;
            
        }
        private void btnBuscarTipoProducto_Click(object sender, EventArgs e)
        {
            //FrmTipoProducto frmTipoP = FrmTipoProducto.GetInstance();
            //1 significa que fue llamado desde el formulario producto
            FrmTipoProducto frmTipoP = new FrmTipoProducto();
            frmTipoP.setFlag("1");
            frmTipoP.ShowDialog();



        }

        public void setCategoria(string id, string descripcion) {

            x1 = id;
            y1 = descripcion;
            txtIdCategoria.Text = x1;
            txtDescripcionCategoria.Text = y1;
            //MessageBox.Show(x+"   "+ y);



        }

        public void setProveedor(string id, string descripcion)

        {
            txtIdProveedor.Text = id;
            txtDescripcionProveedor.Text = descripcion;
            x2 = id;
            y2 = descripcion;
           


        }

        public void setFlag(string flag) {
            txtFlag.Text = flag;

        }



        public FrmProducto()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {

        }
        //-----------------------------------------------------------------------------
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            
            try
            {
                //Validamos antes de guardar
                string sresultado = validarDatos();
                if (sresultado == "")
                {
                    //Es importante saber si el guardar procede de una modificacion o de un nuevo registro
                    if (txtIdProducto.Text.Equals(""))//Si se esta creando uno nuevo
                    {

                        Producto producto = new Producto();
                        producto.DescripcionProducto = txtDescripcionProducto.Text;
                        producto.TipoProducto.Id = Convert.ToInt32(txtIdCategoria.Text);
                        producto.CantidadDisponible= Convert.ToInt32(txtCantidad.Text);
                        producto.Precio = Convert.ToInt32(txtPrecio.Text);
                        producto.Proveedor.Nit = Convert.ToInt32(txtIdProveedor.Text);
                   
                        if (FProducto.Insertar(producto) > 0)
                        {
                            MessageBox.Show("Datos insertados correctamente");
                            //Llamamos el formulario load
                            //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                            //Mostrar nuevamente actualizado los registros
                            FrmProducto_Load(null, null);


                        }
                    }
                    //Si se esta modificando un registro
                    else
                    {
                        //Se hace lo mismo excepto lo de considerar el id
                        Producto producto = new Producto();
                        producto.IdProducto = Convert.ToInt32(txtIdProducto.Text);
                        producto.DescripcionProducto = txtDescripcionProducto.Text;
                        producto.TipoProducto.Id = Convert.ToInt32(txtIdCategoria.Text);
                        producto.CantidadDisponible = Convert.ToInt32(txtCantidad.Text);
                        producto.Precio = Convert.ToInt32(txtPrecio.Text);
                        producto.Proveedor.Nit = Convert.ToInt32(txtIdProveedor.Text);

                        if (FProducto.Actualizar(producto) ==1)
                        {
                            MessageBox.Show("Datos Modificados correctamente");
                            //Llamamos el formulario load
                            //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                            //Mostrar nuevamente actualizado los registros
                            FrmProducto_Load(null, null);


                        }


                    }//Fin del else interno
                }
                else
                {
                    MessageBox.Show("Faltan cargar datos: \n" + sresultado);

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }
        }//Fin boton guardar

        //Creamos un metodo o funcion para ocultar los demas botones innecesarios al realizar una operacion

        public void mostrarGuardarCancelar(Boolean b)
        {
            btnGuardar.Visible = b;
            btnCancelar.Visible = b;
            btnRefresh.Visible = b;
                        
            btnEditar.Visible = !b;
            btnEliminar.Visible = !b;
            btnNuevo.Visible = !b;
            //La grid con los registros tampoco deberia poder modificarse si se esta agregando uno nuevo o editando
            dgvProducto.Enabled = !b;

            //No puedo modificar los datos
            txtDescripcionProducto.Enabled = b;
            txtDescripcionProveedor.Enabled = b;
            txtIdCategoria.Enabled = b;
            txtDescripcionCategoria.Enabled = b;
            txtCantidad.Enabled = b;
            txtPrecio.Enabled = b;
            txtIdProveedor.Enabled = b;
           

            

        }

        //funcion para validar datos obligatorios
        public string validarDatos()
        {
            string resultado = "";
            if (txtDescripcionProducto.Text == "")
            {
                resultado = resultado + "Descripion Producto \n";
            }
            if (txtCantidad.Text == "")
            {

                resultado = resultado + "Cantidad \n";
            }

            return resultado;

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            //Al escoger la opcion de nuevo deberia solo ser visible el boton de guardar o cancelar la crecion del nuevo registro
            mostrarGuardarCancelar(true);

            //Limpiamos las casillas
            txtIdProducto.Text = "";
            txtDescripcionProducto.Text = "";
            txtIdCategoria.Text = "";
            txtCantidad.Text = "";
            txtPrecio.Text = "";
            txtIdProveedor.Text = "";
            txtDescripcionCategoria.Text = "";
            txtDescripcionProveedor.Text = "";
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(true);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(false);
            dgvProducto_CellClick(null, null);
        }

        //Cuando seleccionemos debemos pasar los datos del registro a los campos de texto
        private void dgvProducto_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //							

            // dbo.Productos.idTipoProducto,
            //dbo.tipoProducto.descripcionTipoProducto
            //dbo.Productos.cantidadDisponible, dbo.Productos.precio, dbo.Productos.idProveedor, dbo.Proveedores.razonSocial
            if (dgvProducto.CurrentRow != null)
            {
                txtIdProducto.Text= dgvProducto.CurrentRow.Cells["idProducto"].Value.ToString();
                txtDescripcionProducto.Text = dgvProducto.CurrentRow.Cells["descripcionProducto"].Value.ToString();
                txtIdCategoria.Text = dgvProducto.CurrentRow.Cells["idTipoProducto"].Value.ToString();
                txtDescripcionCategoria.Text = dgvProducto.CurrentRow.Cells["DescripcionTipoProducto"].Value.ToString();
                txtCantidad.Text = dgvProducto.CurrentRow.Cells["cantidadDisponible"].Value.ToString();
                txtPrecio.Text = dgvProducto.CurrentRow.Cells["precio"].Value.ToString();
                txtIdProveedor.Text = dgvProducto.CurrentRow.Cells["idProveedor"].Value.ToString();
                txtDescripcionProveedor.Text = dgvProducto.CurrentRow.Cells["RazonSocial"].Value.ToString();
            }
        }

        //Evento proveniente de la grilla
        private void dgvProducto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Verificar que el click vino de la checkbox
            if (e.ColumnIndex == dgvProducto.Columns["Eliminar"].Index)
            {
                DataGridViewCheckBoxCell chkEliminar =
                (DataGridViewCheckBoxCell)dgvProducto.Rows[e.RowIndex].Cells["Eliminar"];
                //Se le pone el valor opuesto seleccionado-->DesSeleccionado
                chkEliminar.Value = !Convert.ToBoolean(chkEliminar.Value);

            }

        }


        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                //Verificamos que realmente quiere eliminar los registros seleccionados
                if (MessageBox.Show("Realmente desea eliminar los productos seleccionados?", "Eliminacion de producto", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    //Recorrer y obtener todos los id de productos donde este checkeado
                    foreach (DataGridViewRow row in dgvProducto.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells["Eliminar"].Value))
                        {
                            //Si esta checkeado
                            Producto producto = new Producto();
                            //NOTA: Verificar que la columna si sea IdProducto---------------
                            producto.IdProducto = Convert.ToInt32(row.Cells["IdProducto"].Value);
                            if (FProducto.Eliminar(producto) != 1)
                            {
                                MessageBox.Show("El producto no pudo ser eliminado", "Eliminacion de producto", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                            }
                            else {
                                MessageBox.Show("El producto  pudo ser eliminado", "Eliminacion de producto", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                            }

                        }


                    }

                    //Volvemos a cargar la lista de registros una vez se han eliminado
                    FrmProducto_Load(null, null);
                }//Fin del if





            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        //Evento buscar
        private void txtBuscar_TextChanged(object sender, EventArgs  e)
        {
            try
            {
                DataView dv = new DataView(dt.Copy());
                dv.RowFilter = cmbBuscar.Text + " Like '%" + txtBuscar.Text + "%'";
                dgvProducto.DataSource = dv;

                if (dv.Count == 0)
                {
                    lblDatosNoEncontrados.Visible = true;
                }
                else
                {
                    lblDatosNoEncontrados.Visible = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);


            }



        }




        private void btnBuscarProveedor_Click(object sender, EventArgs e)
        {
            FrmProveedor frmProveedor = new FrmProveedor();
            frmProveedor.SetFlag("1");
            frmProveedor.ShowDialog();
        }

  

        private void FrmProducto_Load(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = FProducto.GetAll();
                dt = ds.Tables[0];
                dgvProducto.DataSource = dt;
                //Si hay datos no mostramos la etiqueta de "no se encontraron datos" 
                if (dt.Rows.Count > 0)
                {

                    lblDatosNoEncontrados.Visible = false;
                    dgvProducto_CellClick(null, null);

                }
                else
                {
                    lblDatosNoEncontrados.Visible = true;
                }

                mostrarGuardarCancelar(false);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

                throw;
            }



        }

        private void txtIdCategoria_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTel_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void dgvProducto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (txtFlag.Text == "1")
            {
                FrmProductoXFactura f = FrmProductoXFactura.GetInstance();
                if (dgvProducto.CurrentRow != null)
                {
                    Producto producto = new Producto();
                    producto.IdProducto = Convert.ToInt32(dgvProducto.CurrentRow.Cells["idProducto"].Value.ToString());
                    producto.DescripcionProducto = dgvProducto.CurrentRow.Cells["descripcionProducto"].Value.ToString();
                    producto.CantidadDisponible = Convert.ToInt32(dgvProducto.CurrentRow.Cells["cantidadDisponible"].Value.ToString());
                    producto.Precio = Convert.ToInt32(dgvProducto.CurrentRow.Cells["precio"].Value.ToString());
                    f.setProducto (producto);
                    f.Show();
                    Close();
                }

            }
        }



        private void btnRefresh_Click(object sender, EventArgs e)
        {
            txtIdCategoria.Text = x1;
            txtDescripcionCategoria.Text = y1;
            txtIdProveedor.Text = x2;
            txtDescripcionProveedor.Text = y2;
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
