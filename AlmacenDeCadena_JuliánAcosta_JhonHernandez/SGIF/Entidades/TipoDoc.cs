﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Entidades
{
    public class TipoDoc
    {
        private int _id;
        private String _descTipoDoc;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public String TipoDocu
        {
            get { return _descTipoDoc; }
            set { _descTipoDoc = value; }
        }

        public TipoDoc() { }

        public TipoDoc(int id, string descTipoDoc)
        {
            _id = id;
            _descTipoDoc = descTipoDoc;
        }
    }
}
