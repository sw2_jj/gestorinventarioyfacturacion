﻿namespace SGIF.Presentacion
{
    partial class VentasAnuales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ventasAnuales1 = new SGIF.VentasAnuales();
            this.viewVentasAnualesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.viewVentasAnualesTableAdapter = new SGIF.VentasAnualesTableAdapters.ViewVentasAnualesTableAdapter();
            this.añoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalVentasDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ventasAnuales1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewVentasAnualesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.añoDataGridViewTextBoxColumn,
            this.totalVentasDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.viewVentasAnualesBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(130, 141);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 269);
            this.dataGridView1.TabIndex = 0;
            // 
            // ventasAnuales1
            // 
            this.ventasAnuales1.DataSetName = "VentasAnuales";
            this.ventasAnuales1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // viewVentasAnualesBindingSource
            // 
            this.viewVentasAnualesBindingSource.DataMember = "ViewVentasAnuales";
            this.viewVentasAnualesBindingSource.DataSource = this.ventasAnuales1;
            // 
            // viewVentasAnualesTableAdapter
            // 
            this.viewVentasAnualesTableAdapter.ClearBeforeFill = true;
            // 
            // añoDataGridViewTextBoxColumn
            // 
            this.añoDataGridViewTextBoxColumn.DataPropertyName = "Año";
            this.añoDataGridViewTextBoxColumn.HeaderText = "Año";
            this.añoDataGridViewTextBoxColumn.Name = "añoDataGridViewTextBoxColumn";
            // 
            // totalVentasDataGridViewTextBoxColumn
            // 
            this.totalVentasDataGridViewTextBoxColumn.DataPropertyName = "Total Ventas";
            this.totalVentasDataGridViewTextBoxColumn.HeaderText = "Total Ventas";
            this.totalVentasDataGridViewTextBoxColumn.Name = "totalVentasDataGridViewTextBoxColumn";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 30F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(54, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(426, 49);
            this.label1.TabIndex = 1;
            this.label1.Text = "ALMACEN DE CADENA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Monotype Corsiva", 24.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label2.Location = new System.Drawing.Point(150, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(201, 40);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ventas Anuales";
            // 
            // VentasAnuales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "VentasAnuales";
            this.Text = "VentasAnuales";
            this.Load += new System.EventHandler(this.VentasAnuales_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ventasAnuales1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewVentasAnualesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private SGIF.VentasAnuales ventasAnuales1;
        private System.Windows.Forms.BindingSource viewVentasAnualesBindingSource;
        private VentasAnualesTableAdapters.ViewVentasAnualesTableAdapter viewVentasAnualesTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn añoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalVentasDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}