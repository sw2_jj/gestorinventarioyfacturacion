﻿namespace SGIF
{
    partial class FrmReporteFactura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.usp_Reportes_GenerarReporteFacturaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DataSetPrincipal = new SGIF.DataSetPrincipal();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.usp_Reportes_GenerarReporteFacturaTableAdapter = new SGIF.DataSetPrincipalTableAdapters.usp_Reportes_GenerarReporteFacturaTableAdapter();
            this.txtFacturaId = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.usp_Reportes_GenerarReporteFacturaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSetPrincipal)).BeginInit();
            this.SuspendLayout();
            // 
            // usp_Reportes_GenerarReporteFacturaBindingSource
            // 
            this.usp_Reportes_GenerarReporteFacturaBindingSource.DataMember = "usp_Reportes_GenerarReporteFactura";
            this.usp_Reportes_GenerarReporteFacturaBindingSource.DataSource = this.DataSetPrincipal;
            // 
            // DataSetPrincipal
            // 
            this.DataSetPrincipal.DataSetName = "DataSetPrincipal";
            this.DataSetPrincipal.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.usp_Reportes_GenerarReporteFacturaBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "SGIF.Reportes.RptReporteFactura.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(1020, 492);
            this.reportViewer1.TabIndex = 0;
            this.reportViewer1.Load += new System.EventHandler(this.reportViewer1_Load);
            // 
            // usp_Reportes_GenerarReporteFacturaTableAdapter
            // 
            this.usp_Reportes_GenerarReporteFacturaTableAdapter.ClearBeforeFill = true;
            // 
            // txtFacturaId
            // 
            this.txtFacturaId.Location = new System.Drawing.Point(640, 205);
            this.txtFacturaId.Name = "txtFacturaId";
            this.txtFacturaId.Size = new System.Drawing.Size(100, 20);
            this.txtFacturaId.TabIndex = 1;
            this.txtFacturaId.Visible = false;
            this.txtFacturaId.TextChanged += new System.EventHandler(this.txtFacturaId_TextChanged);
            // 
            // FrmReporteFactura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 492);
            this.Controls.Add(this.txtFacturaId);
            this.Controls.Add(this.reportViewer1);
            this.Name = "FrmReporteFactura";
            this.Text = "FrmReporteFactura";
            this.Load += new System.EventHandler(this.FrmReporteFactura_Load);
            ((System.ComponentModel.ISupportInitialize)(this.usp_Reportes_GenerarReporteFacturaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSetPrincipal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource usp_Reportes_GenerarReporteFacturaBindingSource;
        private DataSetPrincipal DataSetPrincipal;
        private DataSetPrincipalTableAdapters.usp_Reportes_GenerarReporteFacturaTableAdapter usp_Reportes_GenerarReporteFacturaTableAdapter;
        private System.Windows.Forms.TextBox txtFacturaId;
    }
}