﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Entidades
{
    class Usuario
    {
        //[id], [Nombre], [Apellido], [DNI], [Direccion], [Telefono], [Usuario], [Password], [Tipo]
        private static int id;
        private static string nombre;
        private static string apellido;
        private static int dni;
        private static string direccion;
        private static string telefono;
        private static string usuarioC;
        private static string password;
        private static string Tipo;

        public static int Id { get => id; set => id = value; }
        public static string Nombre { get => nombre; set => nombre = value; }
        public static string Apellido { get => apellido; set => apellido = value; }
        public static int Dni { get => dni; set => dni = value; }
        public static string Direccion { get => direccion; set => direccion = value; }
        public static string Telefono { get => telefono; set => telefono = value; }
        public static string UsuarioC { get => usuarioC; set => usuarioC = value; }
        public static string Password { get => password; set => password = value; }
        public static string Tipo1 { get => Tipo; set => Tipo = value; }
    }
}
