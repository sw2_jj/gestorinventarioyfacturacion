﻿using SGIF.Presentacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Formulario de inicio
            //Application.Run(new MDIPrincipal());
            //Application.Run(new FrmProductosMasVendidos());
            Application.Run(new Frmlogin());

        }
    }
}
