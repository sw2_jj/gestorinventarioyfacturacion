﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Entidades
{
    public class Proveedor
    {
        private int _nit;
        private string _razonSocial;
        private string _representanteLegal;
        private string _direccion;
        private string _telefono;
        private string _fax;
        //Metodos getters and Setters
        public int Nit
        {
            get { return _nit; }
            set { _nit = value; }
        }

        public string RozonSocial
        {
            get { return _razonSocial; }
            set { _razonSocial = value; }
        }

        public string RepresentanteLegal
        {
            get { return _representanteLegal; }
            set { _representanteLegal = value; }
        }


        public string Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }

        public string Telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }

        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }
        
        //Constructor vacio
        public Proveedor() { }

        //Contructor inicializando atributos
        public Proveedor(int nit, string razonSocial, string representanteLegal, string direccion, string telefono, string fax)
        {
            this._nit = nit;
            this._razonSocial = razonSocial;
            this._representanteLegal = representanteLegal;
            this._direccion = direccion;
            this._telefono = telefono;
            this._fax = fax;
        }
    }
}
