﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Entidades
{
    class Cargo
    {
        //idCargo, descripcionCargo, salario
        private int idCargo;
        private string descripcionCargo;
        private int salario;

        //Constructores
        public Cargo() { }

        public Cargo(int idCargo, string descripcionCargo, int salario)
        {
            this.idCargo = idCargo;
            this.descripcionCargo = descripcionCargo;
            this.salario = salario;
        }
        //Encapsulamiento
        public int IdCargo { get => idCargo; set => idCargo = value; }
        public string DescripcionCargo { get => descripcionCargo; set => descripcionCargo = value; }
        public int Salario { get => salario; set => salario = value; }


    }
}
