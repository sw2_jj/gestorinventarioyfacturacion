﻿using SGIF.Entidades;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Presentacion
{
    public partial class MDIPrincipal : Form
    {
        private int childFormNumber = 0;

        public MDIPrincipal()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Ventana " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void MDIPrincipal_Load(object sender, EventArgs e)
        {
          
            //Tipos de cuenta almacen,venta, administracion
            if (Usuario.Tipo1 == "Almacen")
            {
                
                productosToolStripMenuItem.Visible = false;
                productosMasVendidosToolStripMenuItem.Visible = false;
                
                produtosPorProveedorToolStripMenuItem.Visible = false;
                ventasToolStripMenuItem.Visible = false;
                trabajadoresToolStripMenuItem.Visible = false;
                cargosToolStripMenuItem.Visible = false;
                ventasToolStripMenuItem.Visible = false;
                ventasToolStripMenuItem.Visible = false;
                clientesToolStripMenuItem.Visible = false;
                productosToolStripMenuItem.Visible = false;
                categoriasToolStripMenuItem.Visible = false;

                // productosToolStripMenuItem1.Visible = false;
                //tiposProductosToolStripMenuItem.Visible = false;


            }
            else if (Usuario.Tipo1 == "Venta")
            {
                //productoPorFacturaToolStripMenuItem.Visible = false;
                productosToolStripMenuItem.Visible = false;
                productosMasVendidosToolStripMenuItem.Visible = false;
                
                produtosPorProveedorToolStripMenuItem.Visible = false;
                
                trabajadoresToolStripMenuItem.Visible = false;
                cargosToolStripMenuItem.Visible = false;
               
                //clientesToolStripMenuItem.Visible = false;
                productosToolStripMenuItem.Visible = false;
                categoriasToolStripMenuItem.Visible = false;

                // productosToolStripMenuItem1.Visible = false;
                tiposProductosToolStripMenuItem.Visible = false;
                masReportesToolStripMenuItem.Visible = false;
                productosToolStripMenuItem1.Visible = false;



            }
            else if (Usuario.Tipo1 == "Administracion") {
                //productoPorFacturaToolStripMenuItem.Visible = false;
                //productosToolStripMenuItem.Visible = false;
                //ventasToolStripMenuItem.Visible = false;
                productosToolStripMenuItem1.Visible = false;
               
                ventasToolStripMenuItem.Visible = false;
                //categoriasToolStripMenuItem.Visible = false;
                tiposProductosToolStripMenuItem.Visible = false;
                //productosToolStripMenuItem1.Visible = false;
               // tiposProductosToolStripMenuItem.Visible = false;



            }
           


        }

        private void windowsMenu_Click(object sender, EventArgs e)
        {

        }

        private void ventasToolStripMenuItem1_Click(object sender, EventArgs e)
        {
           
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCliente cl = new FrmCliente();
            cl.MdiParent = this;
            cl.Show();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmProveedor pr = new FrmProveedor();
            pr.MdiParent = this;
            pr.Show();
        }

        private void categoriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmTipoDocumento td = new FrmTipoDocumento();
            td.MdiParent = this;
            td.Show();
        }

        private void productosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmProducto prod = new FrmProducto();
            prod.MdiParent = this;
            prod.Show();
        }

        private void tiposProductosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmTipoProducto tp = new FrmTipoProducto();
            tp.MdiParent = this;
            tp.Show();
        }

        private void ventasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura fc = new FrmFactura();
            fc.MdiParent = this;
            fc.Show();
        }

       
        private void trabajadoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmTrabajador trab = new FrmTrabajador();
            trab.MdiParent = this;
            trab.Show();
        }

        private void cargosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCargo carg = new FrmCargo();
            carg.MdiParent = this;
            carg.Show();
        }

        private void masReportesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VentasAnuales va = new VentasAnuales();
            va.MdiParent = this;
            va.Show();
        }

        private void productosPorIngresosToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void productosMasVendidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
             FrmProductosMasVendidos pmv = new FrmProductosMasVendidos();
             pmv.MdiParent = this;
             pmv.Show();
        }

        private void produtosPorProveedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductosPorProveedor ppp = new ProductosPorProveedor();
            ppp.MdiParent = this;
            ppp.Show();
        }

        private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
