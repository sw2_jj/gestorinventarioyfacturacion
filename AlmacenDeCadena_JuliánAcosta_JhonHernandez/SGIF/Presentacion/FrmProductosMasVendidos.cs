﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Presentacion
{
    public partial class FrmProductosMasVendidos : Form
    {
        public FrmProductosMasVendidos()
        {
            InitializeComponent();
        }

        private void FrmProductosMasVendidos_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'productosMasVendidos.productosMasVendidos' Puede moverla o quitarla según sea necesario.
            this.productosMasVendidosTableAdapter.Fill(this.productosMasVendidos.productosMasVendidos);
            // TODO: esta línea de código carga datos en la tabla 'productosPorProveedor.Productos_x_Proveedor' Puede moverla o quitarla según sea necesario.
            this.productos_x_ProveedorTableAdapter.Fill(this.productosPorProveedor.Productos_x_Proveedor);

        }
    }
}
