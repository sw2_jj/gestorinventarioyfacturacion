﻿using SGIF.Entidades;
using SisVenttas.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Datos
{
    class FLogin
    {
        public static DataSet ValidarLogin(string sUsuario,String sPassword)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {
                    FDBHelper.MakeParam("@usuario",SqlDbType.VarChar,0,sUsuario),
                    FDBHelper.MakeParam("@password",SqlDbType.VarChar,0,sPassword)
                };
            return FDBHelper.ExecuteDataSet("usp_Data_FLogin_ValidarLogin", dbParams);

        }



    }
}
