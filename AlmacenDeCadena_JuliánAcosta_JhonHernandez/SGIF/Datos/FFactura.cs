﻿using SGIF.Entidades;
using SisVenttas.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Datos
{
    public static class FFactura
    {
        public static DataSet GetAll()
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {

                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            return FDBHelper.ExecuteDataSet("usp_Data_FFactura_GetAll", dbParams);

        }

        //metodo Insertar

        public static int Insertar(Factura factura)
        {
            SqlParameter[] dbParams = new SqlParameter[]
         
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idCliente",SqlDbType.Int,0,factura.Cliente.Id),
                    FDBHelper.MakeParam("@fecha",SqlDbType.Date,0,factura.FechaVenta),
                    
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FFactura_Insertar", dbParams));

        }

        //Metodo Actualizar
        public static int Actualizar(Factura factura)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                 //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idFactura",SqlDbType.Int,0,factura.Id),
                     FDBHelper.MakeParam("@idCliente",SqlDbType.Int,0,factura.Cliente.Id),
                    FDBHelper.MakeParam("@fecha",SqlDbType.Date,0,factura.FechaVenta)
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos


            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FFactura_Actualizar", dbParams));

        }

        //Metodo Eliminar
        public static int Eliminar(Factura factura)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idFactura",SqlDbType.Int,0,factura.Id),
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FFactura_Eliminar", dbParams));

        }
    }
}
