﻿using SGIF.Entidades;
using SisVenttas.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Esta clase contendra todos los metodos para obtener y manipular la informacion de 
//la tabla clientes alojada en la base de datos

namespace SGIF.Datos
{
    public class FCliente
    {
        public static DataSet GetAll()
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {
                    
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            return FDBHelper.ExecuteDataSet("usp_Data_FCliente_GetAll", dbParams);

        }

        //metodo Insertar

        public static int Insertar(Cliente cliente)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@Nombre",SqlDbType.VarChar,0,cliente.Nombre),
                    FDBHelper.MakeParam("@Apellido",SqlDbType.VarChar,0,cliente.Apellido),
                    FDBHelper.MakeParam("@telefono",SqlDbType.VarChar,0,cliente.Telefono),
                    FDBHelper.MakeParam("@direccion",SqlDbType.VarChar,0,cliente.Direccion),
                    FDBHelper.MakeParam("@tipoDocumento",SqlDbType.VarChar,0,cliente.TipoDoc),
                    FDBHelper.MakeParam("@numeroDocumento",SqlDbType.VarChar,0,cliente.NumeroDoc)
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FCliente_Insertar", dbParams));

        }

        //Metodo Actualizar
        public static int Actualizar(Cliente cliente)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idCliente",SqlDbType.Int,0,cliente.Id),
                    FDBHelper.MakeParam("@Nombre",SqlDbType.VarChar,0,cliente.Nombre),
                    FDBHelper.MakeParam("@Apellido",SqlDbType.VarChar,0,cliente.Apellido),
                    FDBHelper.MakeParam("@telefono",SqlDbType.VarChar,0,cliente.Telefono),
                    FDBHelper.MakeParam("@direccion",SqlDbType.VarChar,0,cliente.Direccion),
                    FDBHelper.MakeParam("@tipoDocumento",SqlDbType.Int,0,cliente.TipoDoc),
                    FDBHelper.MakeParam("@numeroDocumento",SqlDbType.Int,0,cliente.NumeroDoc)
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

           
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FCliente_Actualizar", dbParams));

        }

        //Metodo Eliminar
        public static int Eliminar(Cliente cliente)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@id",SqlDbType.Int,0,cliente.Id),
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FCliente_Eliminar", dbParams));

        }


    }
}
