﻿using SGIF.Datos;
using SGIF.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SGIF.Presentacion
{
    public partial class FrmTipoDocumento : Form
    {
        public static DataTable dt = new DataTable();
        public FrmTipoDocumento()
        {
            InitializeComponent();
        }

        private void FrmTipoDocumento_Load(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = FTipoDocumento.GetAll();
                dt = ds.Tables[0];
                dgvTipoDoc.DataSource = dt;
                //Si hay datos no mostramos la etiqueta de "no se encontraron datos" 
                if (dt.Rows.Count > 0)
                {

                    lblDatosNoEncontrados.Visible = false;
                    dgvTipoDoc_CellClick(null, null);

                }
                else
                {
                    lblDatosNoEncontrados.Visible = true;
                }

                mostrarGuardarCancelar(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

                throw;
            }

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                //Validamos antes de guardar
                string sresultado = validarDatos();
                if (sresultado == "")
                {
                    //Es importante saber si el guardar procede de una modificacion o de un nuevo registro
                    if (txtId.Text.Equals(""))//Si se esta creando uno nuevo
                    {

                        TipoDoc tipoDocumento = new TipoDoc();
                        tipoDocumento.TipoDocu = txtNombre.Text;


                        if (FTipoDocumento.Insertar(tipoDocumento) > 0)
                        {
                            MessageBox.Show("Datos insertados correctamente");
                            //Llamamos el formulario load
                            //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                            //Mostrar nuevamente actualizado los registros
                            FrmTipoDocumento_Load(null, null);


                        }
                    }
                    //Si se esta modificando un registro
                    else
                    {
                        //Se hace lo mismo excepto lo de considerar el id
                        TipoDoc tipoDocumento = new TipoDoc();
                        tipoDocumento.Id = Convert.ToInt32(txtId.Text);
                        tipoDocumento.TipoDocu = txtNombre.Text;

                        if (FTipoDocumento.Actualizar(tipoDocumento) > 0)
                        {
                            MessageBox.Show("Datos Modificados correctamente");
                            //Llamamos el formulario load
                            //Lo llamamos debido a que acabamos de crear un nuevo formulario y es importante 
                            //Mostrar nuevamente actualizado los registros
                            FrmTipoDocumento_Load(null, null);
                        }
                    }//Fin del else interno
                }
                else
                {
                    MessageBox.Show("Faltan cargar datos: \n" + sresultado);
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(false);
            dgvTipoDoc_CellClick(null, null);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            //Al escoger la opcion de nuevo deberia solo ser visible el boton de guardar o cancelar la crecion del nuevo registro
            mostrarGuardarCancelar(true);

            //Limpiamos las casillas
            txtId.Text = "";
            txtNombre.Text = "";
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            mostrarGuardarCancelar(true);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                //Verificamos que realmente quiere eliminar los registros seleccionados
                if (MessageBox.Show("Realmente desea eliminar el tipo documento seleccionados?", "Eliminacion de tipo documento", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    //Recorrer y obtener todos los id de tipo producto donde este checkeado
                    foreach (DataGridViewRow row in dgvTipoDoc.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells["Eliminar"].Value))
                        {
                            //Si esta checkeado
                            TipoDoc tipoDocumento = new TipoDoc();
                            //NOTA: Verificar que la columna si sea IdtipoProduc---------------
                            tipoDocumento.Id = Convert.ToInt32(row.Cells["idTipoDocumento"].Value);
                            if (FTipoDocumento.Eliminar(tipoDocumento) != 1)
                            {
                                MessageBox.Show("El tipo de documento no pudo ser eliminado", "Eliminacion de tipo docuemento", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                            }

                        }


                    }

                    //Volvemos a cargar la lista de registros una vez se han eliminado
                    FrmTipoDocumento_Load(null, null);
                }//Fin del if





            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataView dv = new DataView(dt.Copy());
                dv.RowFilter = cmbBuscar.Text + " Like '%" + txtBuscar.Text + "%'";
                dgvTipoDoc.DataSource = dv;

                if (dv.Count == 0)
                {
                    lblDatosNoEncontrados.Visible = true;
                }
                else
                {
                    lblDatosNoEncontrados.Visible = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);


            }
        }

        private void dgvTipoDoc_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Verificar que el click vino de la checkbox
            if (e.ColumnIndex == dgvTipoDoc.Columns["Eliminar"].Index)
            {
                DataGridViewCheckBoxCell chkEliminar =
                (DataGridViewCheckBoxCell)dgvTipoDoc.Rows[e.RowIndex].Cells["Eliminar"];
                //Se le pone el valor opuesto seleccionado-->DesSeleccionado
                chkEliminar.Value = !Convert.ToBoolean(chkEliminar.Value);

            }
        }

        private void dgvTipoDoc_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvTipoDoc.CurrentRow != null)
            {
                txtId.Text = dgvTipoDoc.CurrentRow.Cells[1].Value.ToString();
                txtNombre.Text = dgvTipoDoc.CurrentRow.Cells[2].Value.ToString();
            }
        }
        public void mostrarGuardarCancelar(Boolean b)
        {
            btnGuardar.Visible = b;
            btnCancelar.Visible = b;
            btnEditar.Visible = !b;
            btnEliminar.Visible = !b;
            btnNuevo.Visible = !b;
            //La grid con los registros tampoco deberia poder modificarse si se esta agregando uno nuevo o editando
            dgvTipoDoc.Enabled = !b;

            //No puedo modificar los datos
            txtNombre.Enabled = b;
        }
        //funcion para validar datos obligatorios
        public string validarDatos()
        {
            string resultado = "";
            if (txtNombre.Text == "")
            {
                resultado = resultado + "Nombre \n";
            }

            return resultado;

        }
    }
}
