﻿using SGIF.Entidades;
using SisVenttas.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Datos
{
    public class FTrabajador
    {
        public static DataSet GetAll()
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {

                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            return FDBHelper.ExecuteDataSet("usp_Data_FTrabajador_GetAll", dbParams);

        }

        //metodo Insertar

        public static int Insertar(Trabajador trabajador)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@Nombre",SqlDbType.VarChar,0,trabajador.Nombre),
                    FDBHelper.MakeParam("@Apellido",SqlDbType.VarChar,0,trabajador.Apellido),
                    FDBHelper.MakeParam("@telefono",SqlDbType.VarChar,0,trabajador.Telefono),
                    FDBHelper.MakeParam("@direccion",SqlDbType.VarChar,0,trabajador.Direccion),
                    FDBHelper.MakeParam("@tipoDocumento",SqlDbType.VarChar,0,trabajador.TipoDoc),
                    FDBHelper.MakeParam("@numeroDocumento",SqlDbType.VarChar,0,trabajador.NumeroDoc),
                    FDBHelper.MakeParam("@fechaNacimiento",SqlDbType.Date,0,trabajador.FechaNacimiento)

                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FTrabajador_Insertar", dbParams));

        }

        //Metodo Actualizar
        public static int Actualizar(Trabajador trabajador)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@idTrabajador",SqlDbType.Int,0,trabajador.Id),
                    FDBHelper.MakeParam("@Nombre",SqlDbType.VarChar,0,trabajador.Nombre),
                    FDBHelper.MakeParam("@Apellido",SqlDbType.VarChar,0,trabajador.Apellido),
                    FDBHelper.MakeParam("@telefono",SqlDbType.VarChar,0,trabajador.Telefono),
                    FDBHelper.MakeParam("@direccion",SqlDbType.VarChar,0,trabajador.Direccion),
                    FDBHelper.MakeParam("@tipoDocumento",SqlDbType.Int,0,trabajador.TipoDoc),
                    FDBHelper.MakeParam("@numeroDocumento",SqlDbType.Int,0,trabajador.NumeroDoc),
                    FDBHelper.MakeParam("@fechaNacimiento",SqlDbType.Date,0,trabajador.FechaNacimiento)

                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos


            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FTrabajador_Actualizar", dbParams));

        }

        //Metodo Eliminar
        public static int Eliminar(Trabajador trabajador)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cliente pasada como parametro
                    FDBHelper.MakeParam("@id",SqlDbType.Int,0,trabajador.Id),
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FTrabajador_Eliminar", dbParams));

        }



    }
}
