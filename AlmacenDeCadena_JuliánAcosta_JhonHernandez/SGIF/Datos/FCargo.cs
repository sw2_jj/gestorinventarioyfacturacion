﻿using SGIF.Entidades;
using SisVenttas.Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//Atributos de clase cargo idCargo, descripcionCargo, salario
namespace SGIF.Datos
{
    class FCargo
    {
        public static DataSet GetAll()
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {

                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            return FDBHelper.ExecuteDataSet("usp_Data_FCargo_GetAll", dbParams);

        }

        //metodo Insertar

        public static int Insertar(Cargo cargo)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase cargo pasada como parametro
                    FDBHelper.MakeParam("@descripcionCargo",SqlDbType.VarChar,0,cargo.DescripcionCargo),
                    FDBHelper.MakeParam("@salario",SqlDbType.Int,0,cargo.Salario)

                   };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FCargo_Insertar", dbParams));

        }

        //Metodo Actualizar
        public static int Actualizar(Cargo cargo)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros, los atributos de la clase  pasada como parametro
                    FDBHelper.MakeParam("@idCargo",SqlDbType.Int,0,cargo.IdCargo),
                    FDBHelper.MakeParam("@descripcionCargo",SqlDbType.VarChar,0,cargo.DescripcionCargo),
                    FDBHelper.MakeParam("@salario",SqlDbType.Int,0,cargo.Salario)
                   };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos


            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FCargo_Actualizar", dbParams));

        }

        //Metodo Eliminar
        public static int Eliminar(Cargo cargo)
        {
            SqlParameter[] dbParams = new SqlParameter[]
                {//Para este procedimiento si existen atributos
                    //Hacemos los parametros los atributos de la clase  pasada como parametro
                    FDBHelper.MakeParam("@idCargo",SqlDbType.Int,0,cargo.IdCargo),
                };
            //user store procedure_nombreCarpeta donde esta la clase que invoca, y la clase
            //Invoca el storeProcedure de la base de datos

            //Retornara el ultimo id
            return Convert.ToInt32(FDBHelper.ExecuteScalar("usp_Data_FCargo_Eliminar", dbParams));

        }



    }
}
