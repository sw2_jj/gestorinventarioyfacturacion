﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Entidades
{
    //idProducto, descripcionProducto, idTipoProducto, cantidadDisponible, precio, idProveedor
    public class Producto
    {
        private int _idProducto;
        private string _descripcionProducto;
        private TipoProducto tipoProducto;
        private int _cantidadDisponible;
        private int _precio;
        private Proveedor proveedor;

        public Producto()
        {
            tipoProducto = new TipoProducto();
            proveedor = new Proveedor();
        }

        public Producto(int idProducto, string descripcionProducto, TipoProducto tipoProducto, int cantidadDisponible, int precio, Proveedor proveedor)
        {
            IdProducto = idProducto;
            DescripcionProducto = descripcionProducto;
            TipoProducto = tipoProducto;
            CantidadDisponible = cantidadDisponible;
            Precio = precio;
            Proveedor = proveedor;
        }

        public int IdProducto { get => _idProducto; set => _idProducto = value; }
        public string DescripcionProducto { get => _descripcionProducto; set => _descripcionProducto = value; }
        public TipoProducto TipoProducto { get => tipoProducto; set => tipoProducto = value; }
        public int CantidadDisponible { get => _cantidadDisponible; set => _cantidadDisponible = value; }
        public int Precio { get => _precio; set => _precio = value; }
        public Proveedor Proveedor { get => proveedor; set => proveedor = value; }
        
    }
}
