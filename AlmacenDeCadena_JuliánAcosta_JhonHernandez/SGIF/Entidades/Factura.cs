﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGIF.Entidades
{
    public class Factura
    {
        //idFactura, idCliente, fecha
        private int _idFactura;
        private Cliente _idCliente;
        private DateTime _fecha;
        public Factura()
        {
            _idCliente = new Cliente();
        }
        public Factura(int id, Cliente Cliente, DateTime fechaVenta)
        {
            Id = id;
            Cliente = _idCliente;
            FechaVenta = fechaVenta;
        }

        public int Id { get => _idFactura; set => _idFactura = value; }
        public Cliente Cliente { get => _idCliente; set => _idCliente = value; }
        public DateTime FechaVenta { get => _fecha; set => _fecha = value; }
    }
}
